// Rust will insert this: #version 330 core

/*in vec3 v_normal;
in vec4 v_color;
in vec2 v_uv_0;

uniform vec4 u_ambient_light;
uniform sampler2D u_diffuse_tex;
uniform vec4 u_color;

layout(location = 0) out vec4 r_frag_color;

void main(void) {
    vec4 object_diffuse = v_color * texture(u_diffuse_tex, v_uv);
    vec3 object_normal = normalize(v_normal);

    vec3 diffuse_light = vec3(0.0, 0.0, 0.0);
    for (int i=0; i < 2; i+=1) {
        vec3 light_dir = -directional_lights[i].dir;
        diffuse_light += directional_lights[i].intensity 
            * max(dot(object_normal, light_dir), 0);
    }

    r_frag_color = object_diffuse * vec4(u_ambient_light + diffuse_light);
}*/

/////////////////////////////////////////////

// TODO: Most of this isn't tested yet!

layout(location = 0) out vec4 r_frag_color;

#if defined(SurfaceNormals_PerPixel) || defined(SurfaceColor_Textured)
    in vec2 v_uv0;
#endif

// Surface normal
#if !defined(Shading_Unlit)
    #if defined(SurfaceNormals_PerVertex)

        in vec3 v_normal;

        vec3 surface_normal(void) {
            return v_normal;
        }

    #elif defined(SurfaceNormals_PerPixel)

        // note: UV may be shared with diffuse
        in vec3 v_normal;
        in vec3 v_tangent;

        vec3 surface_normal(void) {
            vec3 sample = texture(u_normal_map, v_uv0).xyz;
            // after interpolation these may no longer be normalized
            vec3 normal = normalize(v_normal);
            vec3 tangent = normalize(v_tangent);
            // TODO: Do we need to normalize this as well?
            vec3 bitangent = cross(normal, tangent);
            mat3 tbn = mat3(tangent, bitangent, normal);
            return tbn * sample;
        }

    #else

        vec3 surface_normal(void) {
            return vec3(0.0, 0.0, -1.0);
        }

    #endif
#else

    vec3 surface_normal(void) {
        return vec3(0.0, 0.0, -1.0);
    }

#endif

// Lighting
#if !defined(Shading_Unlit)
    uniform vec3 u_ambient_light;

    uniform int u_directional_light_count;

    uniform vec3 u_directional_light_dir[2];
    uniform vec3 u_directional_light_intensity[2];

    // Assumes MAX 2 directional lights
    vec3 lighting_diffuse(vec3 object_normal) {
        vec3 diffuse_light = vec3(0.0, 0.0, 0.0);
        for (int i=0; i < 2 && i < u_directional_light_count; i+=1) {
            vec3 light_dir = -u_directional_light_dir[i];
            vec3 intensity = u_directional_light_intensity[i];
            diffuse_light += intensity * max(dot(object_normal, light_dir), 0);
        }
        return diffuse_light;
    }

    vec3 lighting_base(void) {
        return u_ambient_light;
    }
#else
    // unlit
    vec3 lighting_diffuse(vec3 object_normal) {
        return vec3(0.0, 0.0, 0.0);
    }

    vec3 lighting_base(void) {
        return vec3(1.0, 1.0, 1.0);
    }
#endif

// Surface Color
#if defined(SurfaceColor_Solid)

    uniform vec4 u_color;

    vec4 surface_color(void) {
        return u_color;
    }

#elif defined(SurfaceColor_Textured)

    uniform sampler2D u_diffuse_tex;

    vec4 surface_color(void) {
        return texture(u_diffuse_tex, v_uv0);
    }

#else

    vec4 surface_color(void) {
        return vec4(0.8, 0.8, 0.8, 1.0);
    }

#endif

// Vertex Colors
#if defined(Vertex_Colors)
    in vec4 v_color;

    vec4 vertex_color(void) {
        return v_color;
    }
#else
    vec4 vertex_color(void) {
        return vec4(1.0, 1.0, 1.0, 1.0);
    }
#endif

void main(void) {
    vec3 diffuse = lighting_diffuse(surface_normal());
    vec4 object_diffuse = vertex_color() * surface_color();
    vec3 ambient = lighting_base();
    r_frag_color = object_diffuse * vec4(ambient + diffuse, 1.0);
}
