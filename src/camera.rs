//! Utilities for working with cameras.

/// Fov is vertical fov.
pub fn perspective(fov_degrees: f32, aspect: f32, near: f32, far: f32) -> [[f32; 4]; 4] {
    // asserts adapted from cgmath
    assert!(
        fov_degrees > 0.0,
        "The vertical field of view cannot be below zero, found: {:?}",
        fov_degrees
    );
    assert!(
        fov_degrees < 180.0,
        "The vertical field of view cannot be greater than a half turn, found: {:?}",
        fov_degrees
    );
    assert!(
        aspect > 0.0,
        "The aspect ratio cannot be below zero, found: {:?}",
        aspect
    );
    assert!(
        near > 0.0,
        "The near plane distance cannot be below zero, found: {:?}",
        near
    );
    assert!(
        far > 0.0,
        "The far plane distance cannot be below zero, found: {:?}",
        far
    );
    assert!(
        far > near,
        "The far plane cannot be closer than the near plane, found: far: {:?}, near: {:?}",
        far,
        near
    );

    let half_fov = (fov_degrees * ::std::f32::consts::PI / 180.0) / 2.0;
    // r and t stand for frustum "right" and frustum "top" respectively, i.e. the half sizes of the
    // frustum

    // [find t graphically: draw tan/sec/1 as a similar triangle to sin/1/cos
    //  t is immediately seen to be n * tan, where n is the near plane, i.e. the radius of your
    //  "trig circle"]

    let f = 1.0 / half_fov.tan();
    // y = n / t; t = h/2 = n * tan(fov/2); therefore n / t = cot(fov) = f
    // x = n / r; r / t = w / h = a => r = a * t => x = n / (a * n * tan(fov/2)) = cot(...) / a
    let x = f / aspect;
    let y = f;
    // out_w * z is used for perspective division
    let out_w = -1.0;

    // maps z to [-1 to 1]
    let z_scale = (far + near) / (near - far);
    let z_shift = (2.0 * far * near) / (near - far);
    // return matrix laid out column after column, left to right
    [
        [x, 0., 0., 0.],
        [0., y, 0., 0.],
        [0., 0., z_scale, out_w],
        [0., 0., z_shift, 0.],
    ]
}
