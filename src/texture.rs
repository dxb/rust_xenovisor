use crate::renderer::SamplerRef;

use crate::gl::types::*;
use crate::gl;

pub mod prelude {
    //! Everything needed to create a texture.

    pub use super::{
        TextureKind,
        TextureFormat,
        TextureWrap,
        TextureFilter,
        TextureUploadFormat,
        TextureComponentType,
        Sampler,
    };
}

pub struct Texture {
    pub(crate) gl_id: GLuint,
    pub(crate) kind: TextureKind,
    pub(crate) sampler: SamplerRef,
    pub(crate) width: usize,
    pub(crate) height: usize,
}

pub struct Sampler<W: ToWrapVector> {
    pub wrap: W,
    pub min_filtering: TextureFilter,
    pub mag_filtering: TextureFilter,
    /// TODO: Support more sampler parameters. Consider breaking this out into a TextureMipmaps
    /// struct that exposes the various LOD clamping and bias parameters.
    pub mipmaps: Option<TextureFilter>,
}

pub struct InternalSampler {
    pub(crate) gl_id: GLuint,
    #[allow(unused)]
    params: Sampler<[TextureWrap; 3]>,
}

#[derive(Debug, Copy, Clone)]
pub enum TextureWrap {
    Repeat,
    ClampToEdge,
}

#[derive(Debug, Copy, Clone)]
pub enum TextureFilter {
    Linear,
    Nearest,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum TextureKind {
    Tex2d,
    Tex2dArray,
    TexCubeMap,
}

#[allow(non_snake_case)]
macro_rules! struct_TextureFace {
    (
        $(
            $variant:ident => ($gl_enum:path; $index:expr)
        ),*,
    ) => {
        #[derive(Debug, Copy, Clone)]
        pub enum TextureFace {
            $(
                $variant
            ),*,
        }

        impl TextureFace {
            pub fn as_gl_enum(&self) -> GLenum {
                match self {
                    $(
                        Self::$variant => $gl_enum
                    ),*,
                }
            }

            pub fn from_index(index: u32) -> TextureFace {
                match index {
                    $(
                        $index => Self::$variant
                    ),*,
                    _ => panic!("invalid face index {} - must be < 6", index),
                }
            }
        }
    }
}

#[allow(non_snake_case)]
macro_rules! struct_TextureFormat {
    (
        $(
            $variant:ident => $gl_enum:expr; $sized:expr
        ),*,
    ) => {
        #[derive(Debug, Copy, Clone)]
        pub enum TextureFormat {
            $(
                $variant
            ),*,
        }

        impl TextureFormat {
            pub fn as_gl_enum(&self) -> GLenum {
                match self {
                    $(
                        TextureFormat::$variant => $gl_enum
                    ),*,
                }
            }

            /// Sized formats are required for some texture functions.
            pub fn sized(&self) -> bool {
                match self {
                    $(
                        TextureFormat::$variant => $sized
                    ),*,
                }
            }
        }
    }
}

#[allow(non_snake_case)]
macro_rules! struct_TextureUploadFormat {
    (
        $(
            $variant:ident => ($comps:expr, $gl_enum:expr)
        ),*,
    ) => {
        #[derive(Debug, Copy, Clone)]
        pub enum TextureUploadFormat {
            $(
                $variant (TextureComponentType)
            ),*,
        }

        impl TextureUploadFormat {
            pub fn pixel_size_bytes(&self) -> usize {
                match self {
                    $(
                        TextureUploadFormat::$variant (comp_type) => $comps * comp_type.size_bytes()
                    ),*,
                }
            }

            pub fn as_gl_enum(&self) -> GLenum {
                match self {
                    $(
                        TextureUploadFormat::$variant (_) => $gl_enum
                    ),*,
                }
            }

            pub fn get_comp_type_as_gl_enum(&self) -> GLenum {
                // this is a little dumb but I'm lazy... fix?
                match self {
                    $(
                        TextureUploadFormat::$variant (comp_type) => comp_type.as_gl_enum()
                    ),*,
                }
            }
        }
    }
}

#[allow(non_snake_case)]
macro_rules! struct_TextureComponentType {
    (
        $(
            $variant:ident => ($size_bytes:expr, $gl_enum:expr)
        ),*,
    ) => {
        #[derive(Debug, Copy, Clone)]
        pub enum TextureComponentType {
            $(
                $variant
            ),*,
        }

        impl TextureComponentType {
            pub fn size_bytes(&self) -> usize {
                match self {
                    $(
                        TextureComponentType::$variant => $size_bytes
                    ),*,
                }
            }

            pub fn as_gl_enum(&self) -> GLenum {
                match self {
                    $(
                        TextureComponentType::$variant => $gl_enum
                    ),*,
                }
            }
        }
    }
}

struct_TextureFace! {
    PositiveX => (gl::TEXTURE_CUBE_MAP_POSITIVE_X; 0),
    NegativeX => (gl::TEXTURE_CUBE_MAP_NEGATIVE_X; 1),
    PositiveY => (gl::TEXTURE_CUBE_MAP_POSITIVE_Y; 2),
    NegativeY => (gl::TEXTURE_CUBE_MAP_NEGATIVE_Y; 3),
    PositiveZ => (gl::TEXTURE_CUBE_MAP_POSITIVE_Z; 4),
    NegativeZ => (gl::TEXTURE_CUBE_MAP_NEGATIVE_Z; 5),
}

struct_TextureFormat! {
    Rgb => gl::RGB; false,
    Rgba => gl::RGBA; false,
    Grayscale => gl::RED; false,

    Rgb8 => gl::RGB8; true,
    Rgba8 => gl::RGBA8; true,
    Srgb8 => gl::SRGB8; true,
    Srgba8 => gl::SRGB8_ALPHA8; true,
    Grayscale8 => gl::R8; true,
    Grayscale16 => gl::R16; true,
}

struct_TextureUploadFormat! {
    Rgb => (3, gl::RGB),
    Rgba => (4, gl::RGBA),
    Grayscale => (1, gl::RED),
}

struct_TextureComponentType! {
    U8 => (1, gl::UNSIGNED_BYTE),
    U16 => (2, gl::UNSIGNED_SHORT),
}

impl Texture {
    pub fn size(&self) -> [usize; 2] {
        [self.width, self.height]
    }
}

impl TextureKind {
    pub fn as_gl_enum(&self) -> GLenum {
        match self {
            TextureKind::Tex2d => gl::TEXTURE_2D,
            TextureKind::Tex2dArray => gl::TEXTURE_2D_ARRAY,
            TextureKind::TexCubeMap => gl::TEXTURE_CUBE_MAP,
        }
    }

    pub fn is_array(&self) -> bool {
        match self {
            TextureKind::Tex2d => false,
            TextureKind::Tex2dArray => true,
            TextureKind::TexCubeMap => false,
        }
    }
}

impl TextureWrap {
    pub fn as_gl_enum(&self) -> GLenum {
        match self {
            TextureWrap::Repeat => gl::REPEAT,
            TextureWrap::ClampToEdge => gl::CLAMP_TO_EDGE,
        }
    }
}

impl InternalSampler {
    pub(crate) fn new<W: ToWrapVector>(gl_id: GLuint, x: Sampler<W>) -> Self {
        InternalSampler {
            gl_id,
            params: Sampler {
                mag_filtering: x.mag_filtering,
                min_filtering: x.min_filtering,
                mipmaps: x.mipmaps,
                wrap: x.wrap.to_wrap_vector()
            }
        }
    }
}

pub trait ToWrapVector {
    // less code than a DIY trait alias for Into<[...]>
    fn to_wrap_vector(self) -> [TextureWrap; 3];
}

impl ToWrapVector for [TextureWrap; 3] {
    fn to_wrap_vector(self) -> Self { self }
}

impl ToWrapVector for TextureWrap {
    fn to_wrap_vector(self) -> [TextureWrap; 3] {
        [self, self, self]
    }
}

impl ToWrapVector for (TextureWrap,) {
    fn to_wrap_vector(self) -> [TextureWrap; 3] {
        [self.0, TextureWrap::Repeat, TextureWrap::Repeat]
    }
}

impl ToWrapVector for (TextureWrap, TextureWrap, ) {
    fn to_wrap_vector(self) -> [TextureWrap; 3] {
        [self.0, self.1, TextureWrap::Repeat]
    }
}

impl ToWrapVector for (TextureWrap, TextureWrap, TextureWrap) {
    fn to_wrap_vector(self) -> [TextureWrap; 3] {
        [self.0, self.1, self.2]
    }
}
