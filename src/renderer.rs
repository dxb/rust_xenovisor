use crate::debug::{DebugMessageDispatcher, DebugMessageHandler};
use crate::pool::{
    NamedSlicePool,
    Pool,
    PoolRef,
    OpaqueRef,
};

use crate::resources::render_target::RenderTarget;
use crate::result::ResourceResult;
use crate::rgl;
use crate::mesh::*;
use crate::texture::*;
use crate::material::*;
use crate::resources::render_target_builder::RenderTargetBuilder;
use crate::loader::{MeshBuilder, NewMeshCache};
use crate::mesh_builder::MeshBuilder2;
use crate::texture_builder::TextureBuilder2;
use crate::texture_loader::{TextureBuilder, NewTextureCache};
use crate::blending::BlendEquation;

use crate::gl;
use crate::gl::types::*;
use crate::gl::Gl;

use std::borrow::Borrow;
use std::mem;
use std::ffi::c_void;
use std::ptr::null;

#[derive(Debug, Clone)]
pub struct MaterialRef(OpaqueRef);
pub type TextureRef = PoolRef<Texture>;
pub type MeshRef = PoolRef<Mesh>;
pub type SamplerRef = PoolRef<InternalSampler>;
pub type RenderTargetRef = PoolRef<RenderTarget>;

pub(crate) type ProgramRef<P> = PoolRef<ShaderProgram<P>>;

pub struct Renderer<Mat>
where
    Mat: Material,
{
    pub gl: Gl,
    pub(crate) cache: RendererCache<Mat>,
    pub(crate) new_mesh: NewMeshCache,
    pub(crate) new_texture: NewTextureCache,
}

pub struct RendererCache<Mat>
where
    Mat: Material,
{
    samplers: Pool<InternalSampler>,
    textures: Pool<Texture>,
    materials: Pool<CompiledMaterial<Mat>>,
    programs: Pool<ShaderProgram<Mat::ProgramGenerator>>,
    /// List of meshes.
    meshes: Pool<Mesh>,
    parts: NamedSlicePool<(), MeshPart, u32>,
    vbos: NamedSlicePool<(), GLuint, u32>,
    formats: Pool<AttributeFormat>,
    render_targets: Pool<RenderTarget>,
}

#[derive(Debug, Clone)]
pub struct PassInfo {
    pub mode: PassMode,
    pub backface_culling: bool,
    pub depth_test: bool,
    pub blending: Option<BlendEquation>,
    pub render_target: Option<RenderTargetRef>,
}

#[derive(Debug, Clone)]
pub enum PassMode {
    Triangles,
    Lines,
}

impl<Mat: Material> From<PoolRef<CompiledMaterial<Mat>>> for MaterialRef {
    fn from(from: PoolRef<CompiledMaterial<Mat>>) -> MaterialRef {
        MaterialRef(OpaqueRef::from(from))
    }
}

impl<Mat> Renderer<Mat>
where
    Mat: Material,
{
    pub fn new<F: Fn(&str) -> *const c_void>(gl_get_proc_address: F) -> Self {
        Renderer {
            gl: Gl::load_with(|symbol| gl_get_proc_address(symbol) as *const _),
            new_texture: NewTextureCache::new(),
            new_mesh: NewMeshCache::new(),
            cache: RendererCache::new(),
        }
    }

    /// Create a mesh. (deprecated)
    ///
    /// For now, all meshes must be loaded from the main thread. In the future this may be able to
    /// be changed.
    #[deprecated]
    pub fn create_mesh<F: FnOnce(&mut MeshBuilder<Mat>)>(&mut self, f: F) -> MeshRef {
        debug_assert!(self.new_mesh.is_empty());
        let mesh = {
            let mut builder = MeshBuilder::new(self);
            f(&mut builder);
            // create the Mesh!
            builder.finalize()
        };
        gl_panic_on_error!(&self.gl, "following mesh creation");
        mesh
    }

    /// Create a mesh.
    ///
    /// Create a mesh by specifying the buffers and attributes.
    ///
    /// Call `use_buffer` methods followed by `use_attribute` and optionally
    /// `use_index_buffer*` to specify vertex data.
    pub fn mesh_builder(&mut self) -> MeshBuilder2<Mat> {
        MeshBuilder2::new(self)
    }

    /// Create a texture.
    /// 
    /// Create a texture by specifying the definition and data.
    /// 
    /// Call a `declare` method first, to allocate storage, followed by
    /// the relevant `upload` method for your texture type.
    pub fn texture_builder(&mut self) -> TextureBuilder2<Mat> {
        TextureBuilder2::new(self)
    }

    /// Create a new render target, which can be drawn to.
    pub fn render_target_builder(&mut self) -> ResourceResult<RenderTargetBuilder<Mat>> {
        RenderTargetBuilder::new(self)
    }

    /// Create a new mesh with a single empty part assigned material `mat`.
    pub fn create_mesh_empty(&mut self, mat: &MaterialRef) -> MeshRef {
        debug_assert!(self.new_mesh.is_empty());
        let mesh = {
            let mut builder = MeshBuilder::new(self);
            builder.add(|parts| parts.material(mat.clone()));
            builder.finalize()
        };
        gl_panic_on_error!(&self.gl, "following mesh creation");
        mesh
    }

    /// Replace a set of vertex attributes for `mesh`.
    ///
    /// Clamps the set of elements in the mesh part to no more than the length of buffer.
    ///
    /// # Safety
    ///
    /// This method is unsafe as it relies on buffer_format being a correct description fo buffer.
    pub unsafe fn set_mesh_buffer_from_slice_unsafe<T>(
        &mut self,
        mesh: &MeshRef,
        part: usize,
        attribute: Sva,
        buffer: &[T],
        buffer_format: &BufferFormat,
    ) {
        let mesh = self.cache.meshes.get(mesh)
            .expect("Mesh does not exist");
        let parts = self.cache.parts.get_mut(&mesh.parts)
            .expect("Mesh references part that does not exist");
        let part = &mut parts[part];
        part.constrain_elements(buffer.len());
        // TODO: Make this a method on a mesh part
        let attr_fmt = self.cache.formats.get_mut(&part.attribute_format)
            .expect("Mesh part references attribute format that does not exist");
        let fmt = match attribute {
            Sva::Position => &mut attr_fmt.pos,
            Sva::Normal => &mut attr_fmt.normals,
            Sva::Color => &mut attr_fmt.colors,
            Sva::Uv => &mut attr_fmt.uvs,
        };
        let vbo = match fmt {
            Some(fmt) => {
                // TODO: Support interleaved stuff and simple format changes
                if fmt.1 != *buffer_format {
                    unimplemented!("Can't change mesh format yet!");
                }
                let vbos = self.cache.vbos.get(&part.vbos)
                    .expect("Mesh references a VBO slice that does not exist");
                vbos[fmt.0]
            }
            None => {
                let vbo_id = self.cache.vbos.get(&part.vbos)
                    .expect("Mesh references a VBO slice that does not exist")
                    .len();
                let vbo = rgl::create_buffer(&self.gl)
                    .expect(dbpi!("Failed to create VBO"));
                self.cache.vbos.realloc_and_extend(&part.vbos, vbo);
                *fmt = Some((vbo_id, buffer_format.clone()));
                vbo
            }
        };
        debug_assert_ne!(vbo, 0);
        self.gl.BindBuffer(gl::ARRAY_BUFFER, vbo);
        let buffer_byte_len = mem::size_of_val(buffer);
        upload_mesh_vbo(
            &self.gl,
            &buffer_format,
            buffer.as_ptr(),
            buffer_byte_len,
            buffer.len(),
            gl::ARRAY_BUFFER,
            // TODO: expose this somwhere, but this seems like a fair guess
            gl::STREAM_DRAW,
        );

        // OPTIMIZATION: if doing this every time is a problem we can change it
        debug_assert_ne!(part.vao, 0);
        self.gl.BindVertexArray(part.vao);
        let array_index = attribute.array_index();
        upload_buffer_format(
            &self.gl,
            array_index,
            &buffer_format
        );
        self.gl.BindVertexArray(0);
        self.gl.BindBuffer(gl::ARRAY_BUFFER, 0);
        gl_panic_on_error!(&self.gl, "following GL mesh part vbo update");
    }

    pub fn set_mesh_indices_from_slice<T>(
        &mut self,
        mesh: &MeshRef,
        part: usize,
        buffer: &[T]
    )
    where
        T: IndexBufferTypeInstance,
    {
        let mesh = self.cache.meshes.get(mesh)
            .expect("Mesh does not exist");
        let parts = self.cache.parts.get_mut(&mesh.parts)
            .expect("Mesh references part that does not exist");
        let part = &mut parts[part];
        part.constrain_elements(buffer.len());
        if part.ibo == 0 {
            part.ibo = rgl::create_buffer(&self.gl)
                .expect("Failed to crate IBO");
        }
        debug_assert_ne!(part.ibo, 0);
        unsafe {
            self.gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, part.ibo);
            upload_mesh_vbo(
                &self.gl,
                &BufferFormat {
                    components: 1,
                    kind: T::sva_type(),
                    layout: BufferLayout::Packed,
                    normalized: false,
                },
                buffer.as_ptr(),
                mem::size_of_val(buffer),
                buffer.len(),
                gl::ELEMENT_ARRAY_BUFFER,
                gl::STREAM_DRAW,
            );
            self.gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
        }
        gl_panic_on_error!(&self.gl, "following GL mesh part indicies update");
    }

    pub unsafe fn set_mesh_elements(&mut self, mesh: &MeshRef, part: usize, len: usize) {
        let mesh = self.cache.meshes.get(mesh)
            .expect("Mesh does not exist");
        let parts = self.cache.parts.get_mut(&mesh.parts)
            .expect("Mesh references part that does not exist");
        let part = &mut parts[part];
        part.set_elements(len);
    }

    pub fn delete_mesh(&mut self, m: MeshRef) -> Option<()> {
        let mesh = self.cache.meshes.free(&m)?;
        // NLL will save us (I think)
        {
            let parts = self.cache.parts.get(&mesh.parts)
                .expect(dbpi!("Attempted to delete a mesh that references missing parts"));
            for part in parts {
                unsafe {
                    self.gl.DeleteVertexArrays(1, &part.vao);
                    let vbos = self.cache.vbos.get(&part.vbos)
                        .expect(
                            dbpi!("Attempted to delete a mesh that referneces missing vbos")
                        );
                    self.gl.DeleteBuffers(vbos.len() as _, vbos.as_ptr());
                }
                self.cache.vbos.free(&part.vbos);
            }
        }
        self.cache.parts.free(&mesh.parts);
        Some(())
    }

    pub fn create_material(&mut self, material: Mat) -> MaterialRef {
        // OPTIMIZATION: if we want to cache individual shaders, work with the cache inside the
        // ProgramCompiler

        // share a shader program if it already exists
        let program = {
            let generator = material.program_generator();
            self.cache.programs.find_ref(
                |ShaderProgram { generator: g, .. }| *g == *generator
            ).unwrap_or_else(|| {
                // we need to create a program
                let mut compiler = ProgramCompiler::new(&self.gl);
                generator.generate(&mut compiler);
                let program = compiler.compile();
                self.cache.programs.alloc(
                    ShaderProgram::new::<Mat>(&self.gl, program, generator.into_owned())
                )
            })
        };
        let mat = CompiledMaterial::new(program, material);
        self.cache.materials.alloc(mat).into()
    }

    pub fn create_sampler<W: ToWrapVector + Copy>(&mut self, sampler: Sampler<W>) -> SamplerRef {
        use self::TextureFilter::*;
        let min_filtering = match sampler.mipmaps {
            Some(Linear) => match sampler.min_filtering {
                Linear => gl::LINEAR_MIPMAP_LINEAR,
                Nearest => gl::NEAREST_MIPMAP_LINEAR,
            },
            Some(Nearest) => match sampler.min_filtering {
                Linear => gl::LINEAR_MIPMAP_NEAREST,
                Nearest => gl::NEAREST_MIPMAP_NEAREST,
            },
            None => match sampler.min_filtering {
                Linear => gl::LINEAR,
                Nearest => gl::NEAREST,
            },
        };
        let mag_filtering = match sampler.min_filtering {
            Linear => gl::LINEAR,
            Nearest => gl::NEAREST,
        };
        let wrap = sampler.wrap.to_wrap_vector();
        unsafe {
            let mut gl_id = 0;
            self.gl.GenSamplers(1, &mut gl_id);
            if gl_id == 0 {
                panic!(dbpi!("Failed to create sampler object"));
            }
            self.gl.SamplerParameteri(gl_id, gl::TEXTURE_MIN_FILTER, min_filtering as _);
            self.gl.SamplerParameteri(gl_id, gl::TEXTURE_MAG_FILTER, mag_filtering as _);

            self.gl.SamplerParameteri(gl_id, gl::TEXTURE_WRAP_S, wrap[0].as_gl_enum() as _);
            self.gl.SamplerParameteri(gl_id, gl::TEXTURE_WRAP_T, wrap[1].as_gl_enum() as _);
            self.gl.SamplerParameteri(gl_id, gl::TEXTURE_WRAP_R, wrap[2].as_gl_enum() as _);

            let sampler = InternalSampler::new(gl_id, sampler);
            self.cache.samplers.alloc(sampler)
        }
    }

    #[deprecated]
    pub fn create_texture<F: FnOnce(&mut TextureBuilder<Mat>)>(&mut self, f: F) -> TextureRef {
        let tex = {
            let mut builder = TextureBuilder { renderer: self };
            f(&mut builder);
            builder.finalize()
        };
        debug_assert!(self.new_texture.is_empty());
        gl_panic_on_error!(&self.gl, "following mesh creation");
        tex
    }

    pub fn clear(&mut self, r: f32, g: f32, b: f32, a: f32) {
        unsafe {
            self.gl.ClearColor(r, g, b, a);
            self.gl.Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        }
    }

    pub fn clear_render_target(&mut self, rt: &RenderTargetRef, r: f32, g: f32, b: f32, a: f32) {
        let rt = self.cache.render_targets.get(&rt)
            .expect("render target referenced in clear operation does not exist");

        unsafe {
            self.gl.BindFramebuffer(gl::FRAMEBUFFER, rt.gl_id);
            self.gl.ClearColor(r, g, b, a);
            self.gl.Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            self.gl.BindFramebuffer(gl::FRAMEBUFFER, 0);
        }
    }

    pub fn clear_depth(&mut self) {
        unsafe {
            self.gl.Clear(gl::DEPTH_BUFFER_BIT);
        }
    }

    pub fn clear_depth_render_target(&mut self, rt: &RenderTargetRef) {
        let rt = self.cache.render_targets.get(&rt)
            .expect("render target referenced in clear operation does not exist");

        unsafe {
            self.gl.BindFramebuffer(gl::FRAMEBUFFER, rt.gl_id);
            self.gl.Clear(gl::DEPTH_BUFFER_BIT);
            self.gl.BindFramebuffer(gl::FRAMEBUFFER, 0);
        }
    }

    /// Configure the system with required and recommended settings, if supported.
    /// 
    /// We can add an argument here to expose settings as needed.
    pub fn prepare(&mut self) {
        unsafe {
            self.gl.Enable(gl::FRAMEBUFFER_SRGB);
            self.gl.Enable(gl::TEXTURE_CUBE_MAP_SEAMLESS);
        }
    }

    /// Enable OpenGL Debug logging
    pub fn register_debug_message_handler<H: DebugMessageHandler + 'static>(&mut self, handler: H) {
        unsafe {
            DebugMessageDispatcher::init();
            DebugMessageDispatcher::register_handler(handler);

            self.gl.Enable(gl::DEBUG_OUTPUT);
            self.gl.DebugMessageCallback(DebugMessageDispatcher::callback(), null());
        }
    }

    pub fn texture(&self, tex: &TextureRef) -> Option<&Texture> {
        self.cache.textures.get(tex)
    }

    pub fn render_target_texture(&self, rt: &RenderTargetRef) -> Option<TextureRef> {
        let rt = self.cache.render_targets.get(rt)?;
        rt.color_attachment
    }

    /// Easy to use but performance depends on a number of factors.
    ///
    /// If `meshes` is only a few meshes (<10) with limited materials, this will perform fine.
    /// This should even perform fine if individual meshes are very large. This method performs
    /// very little batching and hence performance depends on how well sorted the input is.
    ///
    /// # Safety
    ///
    /// This method is marked safe but you may encounter issues if the invariants of other safe
    /// calls to the API were violated. In general, if this method seg faults it's an API bug.
    pub fn render<'a, M, I, S>
    (
        &mut self,
        size: (u32, u32),
        pass: &PassInfo,
        scene: S,
        meshes: I,
    )
    where
        I: IntoIterator<Item=&'a (M, MeshRef)>,
        M: 'a + Borrow<Mat::MeshData>,
        S: Borrow<Mat::SceneData>,
    {
        gl_panic_on_error!(&self.gl, "before render");
        unsafe {
            let gl = &self.gl;
            gl.Viewport(0, 0, size.0 as _, size.1 as _);

            if pass.backface_culling {
                gl.Enable(gl::CULL_FACE);
            } else {
                gl.Disable(gl::CULL_FACE);
            }
            if pass.depth_test {
                gl.Enable(gl::DEPTH_TEST);
            } else {
                gl.Disable(gl::DEPTH_TEST);
            }

            if let Some(blending) = &pass.blending {
                gl.Enable(gl::BLEND);

                let (eq, func) = blending.as_gl_enum();

                gl.BlendEquation(eq);

                if let Some((src, dst)) = func {
                    gl.BlendFunc(src, dst);
                }
            } else {
                gl.Disable(gl::BLEND);
            }

            let mode = match pass.mode {
                PassMode::Triangles => gl::TRIANGLES,
                PassMode::Lines => {
                    // Hard coded for a project... but can't see why you
                    // wouldn't want this, you're probably just prototyping
                    // with lines anyways.
                    gl.Enable(gl::LINE_SMOOTH);
                    gl.LineWidth(1.5);
                    gl::LINES
                },
            };

            if let Some(rt) = pass.render_target {
                let rt = self.cache.render_targets.get(&rt)
                    .expect("render target referenced in pass does not exist");
                gl.BindFramebuffer(gl::FRAMEBUFFER, rt.gl_id);
            }

            for (tfm, mesh) in meshes {
                let mesh = self.cache.meshes.get(mesh).unwrap();
                let parts = self.cache.parts.get(&mesh.parts).unwrap();
                for part in parts {
                    // select the material
                    let material = self.cache.materials.get(&part.material.0.trust_ref())
                        .unwrap();
                    let program = self.cache.programs.get(&material.program).unwrap();
                    gl.UseProgram(program.gl_id);

                    // upload the uniforms

                    // TODO: Upload efficiently:
                    //      - scene uniforms when program changes
                    //      - material uniforms when material changes (by ==)
                    upload_uniform_set(
                        &self.gl,
                        &self.cache,
                        &program.material_uniform_info,
                        &material.material_uniforms,
                    );
                    upload_uniform_set(
                        &self.gl,
                        &self.cache,
                        &program.scene_uniform_info,
                        scene.borrow(),
                    );
                    upload_uniform_set(
                        &self.gl,
                        &self.cache,
                        &program.mesh_uniform_info,
                        tfm.borrow(),
                    );

                    gl_panic_on_error!(&self.gl, "during render, before drawing");

                    // bind the vertex data and draw
                    gl.BindVertexArray(part.vao);

                    if part.ibo != 0 {
                        gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, part.ibo);
                        gl.DrawElements(
                            mode,
                            part.elements as _,
                            part.ibo_type,
                            ::std::ptr::null(),
                        );
                        gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
                    } else {
                        gl.DrawArrays(mode, 0, part.elements as _);
                    }
                }
            }

            gl.BindFramebuffer(gl::FRAMEBUFFER, 0);

            // Only worry about TEXTURE0
            gl.BindVertexArray(0);
            gl.UseProgram(0);

            gl_panic_on_error!(&self.gl, "following render");
        }
    }
}

impl<Mat> RendererCache<Mat>
where
    Mat: Material,
{
    pub fn new() -> Self {
        RendererCache {
            samplers: Pool::new(),
            textures: Pool::new(),
            materials: Pool::new(),
            programs: Pool::new(),
            meshes: Pool::new(),
            parts: NamedSlicePool::new(),
            vbos: NamedSlicePool::new(),
            formats: Pool::new(),
            render_targets: Pool::new(),
        }
    }

    pub(crate) fn assert_material_attrib_compatibility(
        &self,
        mat: &MaterialRef,
        fmt: &AttributeFormatRef
    ) {
        let mat = self.materials.get(&mat.0.trust_ref())
            .expect(dbpi!());
        let fmt = self.formats.get(fmt)
            .expect(dbpi!());
        mat.assert_compatibility(&fmt);
    }

    pub(crate) fn alloc_format(&mut self, fmt: AttributeFormat) -> AttributeFormatRef {
        self.formats.alloc(fmt)
    }

    pub(crate) fn alloc_vbos(&mut self, vbos: &[GLuint]) -> VboRef {
        self.vbos.alloc_from_copy(vbos)
    }

    pub(crate) fn alloc_mesh_parts(&mut self, parts: &mut [Option<MeshPart>]) -> MeshPartRef {
        self.parts.alloc_from_slice(parts)
    }

    pub(crate) fn alloc_mesh(&mut self, mesh: Mesh) -> MeshRef {
        self.meshes.alloc(mesh)
    }

    pub(crate) fn alloc_texture(&mut self, tex: Texture) -> TextureRef {
        self.textures.alloc(tex)
    }

    pub(crate) fn alloc_render_target(&mut self, rt: RenderTarget) -> RenderTargetRef {
        self.render_targets.alloc(rt)
    }
}

impl Default for PassInfo {
    fn default() -> PassInfo {
        PassInfo {
            backface_culling: true,
            depth_test: true,
            mode: PassMode::Triangles,
            blending: None,
            render_target: None,
        }
    }
}

unsafe fn upload_uniform_set<Mat: Material, U: UniformSet>(
    gl: &Gl,
    cache: &RendererCache<Mat>,
    info: &[UniformInfo],
    set: &U,
) {
    // upload uniforms
    for i in info {
        set.derive(|value| {
            (i.method)(gl, i.location, value)
        }, i.field_index)
    }
    // bind textures
    for i in 0..set.tex_count() {
        let (unit, tex) = set.tex_ref(i);
        let tex = cache.textures.get(tex)
            .expect(dbpi!("Failed to get texture from uniform set"));
        gl.ActiveTexture(gl::TEXTURE0 + (unit as GLenum));
        gl.BindTexture(tex.kind.as_gl_enum(), tex.gl_id);
        let sampler = cache.samplers.get(&tex.sampler)
            .expect(dbpi!("Texture references invalid sampler"));
        gl.BindSampler(unit, sampler.gl_id);
    }
}
