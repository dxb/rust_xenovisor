use crate::renderer::{Renderer, SamplerRef, TextureRef};
use crate::material::Material;
use crate::gl::types::*;
use crate::texture::{TextureFormat, TextureUploadFormat, TextureKind, Texture, TextureFace};

use std::cmp;

pub struct TextureBuilder2<'r, M: Material> {
    renderer: &'r mut Renderer<M>,
    kind: Option<TextureKind>,
    // internal_format: Option<TextureFormat>,
    width: u32,
    height: u32,
    sampler: Option<SamplerRef>,
    levels: u32,
    max_declared_level: u32,
    gl_id: GLuint,
}

impl<'r, M: Material> TextureBuilder2<'r, M> {
    pub(crate) fn new(renderer: &mut Renderer<M>) -> TextureBuilder2<M> {
        let gl_id = unsafe {
            let mut tex = 0;
            renderer.gl.GenTextures(1, &mut tex);
            if tex == 0 {
                panic!(dbpi!("Failed to create a new texture"));
            }
            tex
        };

        TextureBuilder2 {
            renderer,
            kind: None,
            // internal_format: None,
            width: 0,
            height: 0,
            sampler: None,
            levels: 0,
            max_declared_level: 0,
            gl_id,
        }
    }

    pub fn declare(
        &mut self,
        kind: TextureKind,
        internal_format: TextureFormat,
        mip_map_levels: u32,
        width: u32,
        height: u32,
        sampler: SamplerRef,
    ) {
        self.declare_inner(
            kind,
            internal_format,
            mip_map_levels,
            width,
            height,
            0,
            sampler,
        );
    }

    pub fn declare_array(
        &mut self,
        kind: TextureKind,
        internal_format: TextureFormat,
        mip_map_levels: u32,
        width: u32,
        height: u32,
        depth: u32,
        sampler: SamplerRef,
    ) {
        self.declare_inner(
            kind,
            internal_format,
            mip_map_levels,
            width,
            height,
            depth,
            sampler,
        );
    }

    fn declare_inner(
        &mut self,
        kind: TextureKind,
        internal_format: TextureFormat,
        mip_map_levels: u32,
        width: u32,
        height: u32,
        depth: u32,
        sampler: SamplerRef,
    ) {
        assert!(width < i32::MAX as u32);
        assert!(height < i32::MAX as u32);
        assert!(mip_map_levels < i32::MAX as u32);
        assert!(depth < i32::MAX as u32);
        assert!(internal_format.sized(), "a sized format is required for immutable storage");

        self.kind = Some(kind);
        self.width = width;
        self.height = height;
        self.sampler = Some(sampler);
        self.levels = mip_map_levels;

        unsafe {
            let target = kind.as_gl_enum();
            self.renderer.gl.BindTexture(target, self.gl_id);

            match kind {
                TextureKind::Tex2d | TextureKind::TexCubeMap => {
                    self.renderer.gl.TexStorage2D(
                        target,
                        mip_map_levels as _,
                        internal_format.as_gl_enum(),
                        width as _,
                        height as _,
                    );
                },
                TextureKind::Tex2dArray => {
                    self.renderer.gl.TexStorage3D(
                        target,
                        mip_map_levels as _,
                        internal_format.as_gl_enum(),
                        width as _,
                        height as _,
                        depth as _,
                    );
                }
            }
        }

        gl_panic_on_error!(&self.renderer.gl, "following texture declaration");
    }

    pub fn upload(
        &mut self,
        buffer: &[u8],
        level: u32,
        upload_format: TextureUploadFormat,
    ) {
        self.max_declared_level = cmp::max(level, self.max_declared_level);

        let (width, height) = validate_upload(
            &self.kind, 
            level, 
            self.max_declared_level, 
            self.levels, 
            self.width, 
            self.height, 
            &upload_format, 
            buffer,
        );

        let kind = self.kind.as_ref().unwrap();
        assert_eq!(kind.is_array(), false, "upload_layer must be used for array textures");

        unsafe {
            let target = kind.as_gl_enum();
            self.renderer.gl.TexSubImage2D(
                target,
                level as _,
                0,
                0,
                width as _,
                height as _,
                upload_format.as_gl_enum(),
                upload_format.get_comp_type_as_gl_enum(),
                buffer.as_ptr() as *const _,
            )
        }

        gl_panic_on_error!(&self.renderer.gl, "following texture upload");
    }

    pub fn upload_layer(
        &mut self,
        buffer: &[u8],
        layer: u32,
        level: u32,
        upload_format: TextureUploadFormat,
    ) {
        self.max_declared_level = cmp::max(level, self.max_declared_level);

        let (width, height) = validate_upload(
            &self.kind, 
            level, 
            self.max_declared_level, 
            self.levels, 
            self.width, 
            self.height, 
            &upload_format, 
            buffer,
        );

        let kind = self.kind.as_ref().unwrap();
        assert_eq!(kind.is_array(), true, "upload must be used for non-array textures");

        unsafe {
            let target = kind.as_gl_enum();
            self.renderer.gl.TexSubImage3D(
                target,
                level as _,
                0,
                0,
                layer as i32,
                width as _,
                height as _,
                1,
                upload_format.as_gl_enum(),
                upload_format.get_comp_type_as_gl_enum(),
                buffer.as_ptr() as *const _,
            );
        }

        gl_panic_on_error!(&self.renderer.gl, "following texture upload");
    }

    pub fn upload_face(
        &mut self,
        buffer: &[u8],
        face: TextureFace,
        level: u32,
        upload_format: TextureUploadFormat,
    ) {
        let kind = self.kind.as_ref().unwrap();
        assert_eq!(*kind, TextureKind::TexCubeMap, "upload_face is for cube maps");

        self.max_declared_level = cmp::max(level, self.max_declared_level);

        let (width, height) = validate_upload(
            &self.kind, 
            level, 
            self.max_declared_level, 
            self.levels, 
            self.width, 
            self.height, 
            &upload_format, 
            buffer,
        );
        
        unsafe {
            let target = face.as_gl_enum();
            self.renderer.gl.TexSubImage2D(
                target,
                level as _,
                0,
                0,
                width as _,
                height as _,
                upload_format.as_gl_enum(),
                upload_format.get_comp_type_as_gl_enum(),
                buffer.as_ptr() as *const _,
            );
        }

        gl_panic_on_error!(&self.renderer.gl, "following texture upload");
    }

    pub fn build(self) -> TextureRef {
        let renderer = self.renderer;
        unsafe {
            // cleanup
            let target = self.kind.unwrap().as_gl_enum();
            renderer.gl.BindTexture(target, 0);
        }

        assert_eq!(self.max_declared_level + 1, self.levels, "not all mip-map levels declared");

        let tex = Texture {
            gl_id: self.gl_id,
            sampler: self.sampler.unwrap(),
            height: self.height as _,
            width: self.width as _,
            kind: self.kind.unwrap(),
        };

        renderer.cache.alloc_texture(tex)
    }
}

fn validate_upload(
    kind: &Option<TextureKind>,
    level: u32,
    max_declared_level: u32,
    level_count: u32,
    base_width: u32,
    base_height: u32,
    upload_format: &TextureUploadFormat,
    buffer: &[u8],
) -> (u32, u32) {
    assert!(kind.is_some(), "texture must be declared prior to upload");

    assert!(level < level_count);
    assert!(max_declared_level < level_count);

    // calculate the size for this level
    let (width, height) = (
        (base_width as f64 / ((1 << level) as f64))
            .floor() as u32,
        (base_height as f64 / ((1 << level) as f64))
            .floor() as u32,
    );

    let expected_size = width * height * 
        upload_format.pixel_size_bytes() as u32;
    assert_eq!(
        buffer.len(),
        expected_size as usize,
        concat!(
            "Texture buffer size (left) must match the expected size",
            " (right) based on format and mipmap level",
        ),
    );

    (width, height)
}