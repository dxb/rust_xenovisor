use crate::mesh::*;
use crate::material::Material;
use crate::renderer::{Renderer, MeshRef, MaterialRef};
use crate::rgl;

use crate::gl::types::*;
use crate::gl;


pub(crate) struct NewMeshCache {
    parts: Vec<Option<MeshPart>>,
    vbos: Vec<GLuint>,
    sources: Vec<(Sva, ExtractMethod)>,
    material: Option<MaterialRef>,
    ibo: Option<ExtractIbo>,
}

/// Entry point for create_mesh/mesh initialization.
pub struct MeshBuilder<'a, M: Material> { renderer: &'a mut Renderer<M> }

pub struct Extractor<'a, M: Material> { renderer: &'a mut Renderer<M> }

enum ExtractMethod {
    Buffer(ExtractBuffer),
}

struct ExtractBuffer {
    buffer: *const u8,
    buffer_byte_len: usize,
    elements: usize,
    format: BufferFormat,
}

struct ExtractIbo {
    buffer: *const u8,
    buffer_byte_len: usize,
    elements: usize,
    kind: SvaType,
}

impl NewMeshCache {
    pub(crate) fn new() -> Self {
        NewMeshCache {
            parts: Vec::new(),
            vbos: Vec::new(),
            sources: Vec::new(),
            material: None,
            ibo: None,
        }
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.parts.is_empty()
            && self.vbos.is_empty()
            && self.sources.is_empty()
            && self.material.is_none()
            && self.ibo.is_none()
    }

    /// Pushes sources into parts buffer and clears the sources.
    ///
    /// This is in the NewMeshCache impl because it is only relevant when interacting with the
    /// temporary store, although it is more physically associated with the renderer, since it
    /// mutates every part of the renderer! So it is here for code locality but an associated method
    /// because it's more of a renderer's method.
    ///
    /// # Safety
    ///
    /// The safety of this function depends on the user verifying their source data isn't garbage.
    unsafe fn new_part<M: Material>(renderer: &mut Renderer<M>) {
        let cache = &mut renderer.cache;
        let gl = &renderer.gl;
        let mut attribute_format = AttributeFormat {
            pos: None,
            normals: None,
            colors: None,
            uvs: None,
        };

        // create a vao
        let vao = rgl::create_vao(&renderer.gl)
            .unwrap();
        gl.BindVertexArray(vao);

        // OpenGL will require an integer for array length
        // we use -1 as semantic "None"/unset for no elements specified
        // (the elements in a mesh = min(vertices, indices), if we set this to 0 it will never be
        // nonzero because of the min operation and we cannot distinguish between 0 and unset)
        let mut elements: isize = -1;

        debug_assert!(renderer.new_mesh.vbos.is_empty());

        // load each source into a VBO and/or declare the format of a VBO
        for (attr, method) in &renderer.new_mesh.sources {
            let fmt = match attr {
                Sva::Position => &mut attribute_format.pos,
                Sva::Normal => &mut attribute_format.normals,
                Sva::Color => &mut attribute_format.colors,
                Sva::Uv => &mut attribute_format.uvs,
            };
            // Check if this attribute has already been declared
            if fmt.is_some() {
                panic!("MeshPart declared {:?} attribute more than once!", attr);
            }
            // Attempt to load the source into GPU memory
            match method {
                ExtractMethod::Buffer(src) => {
                    // TODO: Add some method of manipulating incoming buffer formats
                    // TODO: Add dynamic meshes, create client side memory, and lazily load them
                    // into the GPU at render-time

                    /*
                        TODO: Interleaved. User should declare which attributes are grouped
                        together. User then should then specify their desired formats. If the
                        formats match AND the relevant interleaved buffers have the same
                        `src.buffer`, then we can read directly into GPU memory... this will likely
                        require multiple passes on `new_mesh.sources`. Strategy pattern? (i.e., if
                        user requests interleaved buffers, use two pass method and expect all
                        buffers (or just the relevant buffers) to be sourced with `ExtractBuffer`.
                        Then copy if that passes. Else, do it this way and when interleaved, fall
                        back to an iterator.
                     */
                    let vbo = rgl::create_buffer(&renderer.gl)
                       .expect(dbpi!("Failed to create VBO"));
                    *fmt = Some((renderer.new_mesh.vbos.len(), src.format));
                    renderer.new_mesh.vbos.push(vbo);

                    gl.BindBuffer(gl::ARRAY_BUFFER, vbo);

                    // TODO: Can buffers be different lengths or should we require them to all be
                    // the same?
                    // TODO: If indices is specified before VBOs, elements will have smallest VBO size
                    //      If indices is specified last, indices has the final say on element count
                    //      This is probably(!) the correct behavior but should be specified somewhere

                    if elements < 0 {
                        // element count is currently unset
                        elements = src.elements as isize;
                    } else {
                        elements = ::std::cmp::min(elements,src.elements as isize);
                    }

                    // TODO: Update the usage when we have dynamic meshes (and make sure IBO usage
                    // flag matches)
                    upload_mesh_vbo(
                        gl,
                        &src.format,
                        src.buffer,
                        src.buffer_byte_len,
                        src.elements,
                        gl::ARRAY_BUFFER,
                        gl::STATIC_DRAW,
                    );

                    let array_index = attr.array_index();
                    upload_buffer_format(gl, array_index, &src.format);
                    gl_panic_on_error!(gl, "following GL mesh part vbo specification");
                }
            };
            debug_assert!(fmt.is_some(), "Every source must result in a format being stored!");
        }
        renderer.new_mesh.sources.clear();

        gl.BindBuffer(gl::ARRAY_BUFFER, 0);
        gl.BindVertexArray(0);

        // check if there is an IBO, and upload it if so
        let (ibo, ibo_type) = if let Some(ibo_desc) = renderer.new_mesh.ibo.take() {
            let ibo = rgl::create_buffer(&renderer.gl)
                .expect(dbpi!("Failed to create IBO"));
            // IBO count is the ultimate arbiter of element count
            elements = ibo_desc.elements as isize;
            gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ibo);
            gl.BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                ibo_desc.buffer_byte_len as _,
                ibo_desc.buffer as *const _,
                gl::STATIC_DRAW,
            );
            gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl_panic_on_error!(gl, "following GL mesh part ibo specification");
            (ibo, ibo_desc.kind.as_glenum())
        } else {
            (0, gl::UNSIGNED_SHORT)
        };

        // add the format to the renderer
        let attribute_format = cache.alloc_format(attribute_format);
        // add the vbos to the renderer
        let vbos = cache.alloc_vbos(&renderer.new_mesh.vbos);
        renderer.new_mesh.vbos.clear();
        // TODO: Default material for prototyping
        let material = renderer.new_mesh.material.take()
            .expect(dbpi!("You must declare exactly one material per part"));
        cache.assert_material_attrib_compatibility(&material, &attribute_format);

        // add the part to the cache
        let part = MeshPart {
            attribute_format,
            vbos,
            vao,
            ibo,
            ibo_type,
            elements,
            material,
        };
        renderer.new_mesh.parts.push(Some(part));
    }
}

impl<'a, M: Material> MeshBuilder<'a, M> {
    pub(crate) fn new(renderer: &'a mut Renderer<M>) -> Self {
        MeshBuilder {
            renderer,
        }
    }

    /// # Safety
    ///
    /// In general this function is safe (excluding bugs), but it becomes possibly unsafe when
    /// unsafe Extractor methods are used. For now, this function will be left safe as a
    /// convenience; if there is potential unsoundness, the API should guarantee that it only comes
    /// from unsafe Extractor usage, in which case the user will have an unsafe block at the source
    /// (no pun intended) of their trouble.
    pub fn add<F: FnOnce(&mut Extractor<M>)>(&mut self, f: F) {
        f(&mut Extractor { renderer: &mut self.renderer });
        // create the mesh parts!
        unsafe { NewMeshCache::new_part(self.renderer) };
    }

    pub(crate) fn finalize(self) -> MeshRef {
        let renderer = self.renderer;
        debug_assert!(renderer.new_mesh.sources.is_empty());
        let parts = renderer.cache.alloc_mesh_parts(&mut renderer.new_mesh.parts);
        renderer.new_mesh.parts.clear();
        renderer.cache.alloc_mesh(Mesh { parts })
    }
}

impl<'a, M: Material> Extractor<'a, M> {
    /// Inform the MeshBuilder that your vertex data is being sourced from an in memory buffer.
    ///
    /// # Safety
    ///
    /// `buffer` must live at least as long as the MeshBuilder.
    ///
    /// This method is unsafe as it has limited ways to protect against invalid memory reads. This
    /// method is safe as long as `len` is actually the length of the buffer in bytes.
    ///
    /// TODO: Could being generic over T open us up to any issues? Should we say something about
    /// alignment/unaligned reads?
    pub unsafe fn buffer<T>(
        &mut self,
        attribute: Sva,
        buffer: *const T,
        buffer_byte_len: usize,
        elements: usize,
        format: BufferFormat,
    ) {
        // TODO: Do the byte length checks here if we can, instead of in the Builder
        format.assert_valid();
        self.renderer.new_mesh.sources.push((
            attribute,
            ExtractMethod::Buffer(ExtractBuffer {
                buffer: buffer as *const _,
                buffer_byte_len,
                elements,
                format,
            }),
        ));
    }

    /// Declare an index buffer.
    ///
    /// Index buffers are currently optional (and will likely remain that way).
    pub unsafe fn indices<T: IndexBufferTypeInstance>(
        &mut self,
        buffer: *const T,
        buffer_byte_len: usize,
        elements: usize,
    ) {
        if self.renderer.new_mesh.ibo.is_some() {
            panic!("A part can only have one index buffer (IBO)!");
        }
        let kind = T::sva_type();
        let anticipated_length = kind.size_bytes() * elements;
        assert_eq!(
            anticipated_length,
            buffer_byte_len,
            "Supplied index buffer length is different from anticipated length",
        );
        self.renderer.new_mesh.ibo = Some(ExtractIbo {
            kind: T::sva_type(),
            buffer: buffer as *const _,
            buffer_byte_len,
            elements,
        });
    }

    pub fn material(&mut self, m: MaterialRef) {
        if self.renderer.new_mesh.material.is_some() {
            panic!("A part can only have one material!");
        }
        self.renderer.new_mesh.material = Some(m);
    }
}
