use crate::gl;
use crate::gl::types::GLenum;

#[derive(Debug, Clone)]
pub enum BlendEquation {
    Add(BlendFunction, BlendFunction),
    Subtract(BlendFunction, BlendFunction),
    ReverseSubtract(BlendFunction, BlendFunction),
    Min,
    Max,
}

#[derive(Debug, Clone)]
pub enum BlendFunction {
    Zero,
    One,

    SrcColor,
    OneMinusSrcColor,
    DstColor,
    OneMinusDstColor,

    SrcAlpha,
    OneMinusSrcAlpha,
    DstAlpha,
    OneMinusDstAlpha,

    SrcAlphaSaturate,
    // ConstantColor,
    // ConstantAlpha,
    // OneMinusConstantColor,
    // OneMinusConstantAlpha,
}

impl BlendEquation {
    pub fn overlay_using_src() -> BlendEquation {
        BlendEquation::Add(BlendFunction::SrcAlpha, BlendFunction::OneMinusSrcAlpha)
    }

    pub fn overlay_premultiplied() -> BlendEquation {
        BlendEquation::Add(BlendFunction::One, BlendFunction::OneMinusSrcAlpha)
    }

    pub(crate) fn as_gl_enum(&self) -> (GLenum, Option<(GLenum, GLenum)>) {
        #[inline(always)]
        fn func(a: &BlendFunction, b: &BlendFunction) -> Option<(GLenum, GLenum)> {
            Some((a.as_gl_enum(), b.as_gl_enum()))
        }

        match self {
            BlendEquation::Add(a, b) => (gl::FUNC_ADD, func(a, b)),
            BlendEquation::Subtract(a, b) => (gl::FUNC_SUBTRACT, func(a, b)),
            BlendEquation::ReverseSubtract(a, b) => (gl::FUNC_REVERSE_SUBTRACT, func(a, b)),
            BlendEquation::Min => (gl::MIN, None),
            BlendEquation::Max => (gl::MAX, None),
        }
    }
}

impl BlendFunction {
    pub(crate) fn as_gl_enum(&self) -> GLenum {
        match self {
            BlendFunction::Zero => gl::ZERO,
            BlendFunction::One => gl::ONE,
            BlendFunction::SrcColor => gl::SRC_COLOR,
            BlendFunction::OneMinusSrcColor => gl::ONE_MINUS_SRC_COLOR,
            BlendFunction::DstColor => gl::DST_COLOR,
            BlendFunction::OneMinusDstColor => gl::ONE_MINUS_DST_COLOR,
            BlendFunction::SrcAlpha => gl::SRC_ALPHA,
            BlendFunction::OneMinusSrcAlpha => gl::ONE_MINUS_SRC_ALPHA,
            BlendFunction::DstAlpha => gl::DST_ALPHA,
            BlendFunction::OneMinusDstAlpha => gl::ONE_MINUS_DST_ALPHA,
            BlendFunction::SrcAlphaSaturate => gl::SRC_ALPHA_SATURATE,
        }
    }
}
