use crate::gl::{types::*, self};

use std::{sync::{Mutex, atomic::{AtomicBool, Ordering}}, ffi::{c_void, CStr}, borrow::Cow};

// we have no dependencies right now... so why bring in lazy static? *shrug*

static mut INITIALIZED: AtomicBool = AtomicBool::new(false);
static mut DEBUG_LOG_DISPATCHER: Option<DebugMessageDispatcher> = None;

pub trait DebugMessageHandler {
    fn handle_message(&self, message: &DebugMessage);
}

#[derive(Debug)]
pub struct DebugMessage<'a> {
    pub source: DebugMessageSource,
    pub severity: DebugMessageSeverity,
    pub text: Cow<'a, str>,
}

#[derive(Debug, Clone, Copy)]
pub enum DebugMessageSource {
    Api,
    Shader,
    Other,
    Unknown,
}

impl DebugMessageSource {
    fn from_gl_enum(value: GLenum) -> DebugMessageSource {
        match value {
            gl::DEBUG_SOURCE_API => DebugMessageSource::Api,
            gl::DEBUG_SOURCE_WINDOW_SYSTEM => DebugMessageSource::Other,
            gl::DEBUG_SOURCE_SHADER_COMPILER => DebugMessageSource::Shader,
            gl::DEBUG_SOURCE_THIRD_PARTY => DebugMessageSource::Other,
            gl::DEBUG_SOURCE_APPLICATION => DebugMessageSource::Other,
            gl::DEBUG_SOURCE_OTHER => DebugMessageSource::Other,
            _ => DebugMessageSource::Unknown,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum DebugMessageSeverity {
    Error,
    Notification,
    Other,
    Unknown,
}

impl DebugMessageSeverity {
    fn from_gl_enum(value: GLenum) -> DebugMessageSeverity {
        match value {
            gl::DEBUG_SEVERITY_HIGH => DebugMessageSeverity::Error,
            gl::DEBUG_SEVERITY_MEDIUM => DebugMessageSeverity::Other,
            gl::DEBUG_SEVERITY_LOW => DebugMessageSeverity::Other,
            gl::DEBUG_SEVERITY_NOTIFICATION => DebugMessageSeverity::Notification,
            _ => DebugMessageSeverity::Unknown,
        }
    }
}

pub struct DebugMessageDispatcher {
    handlers: Mutex<Vec<Box<dyn DebugMessageHandler>>>,
}

impl DebugMessageDispatcher {
    pub unsafe fn init() {
        DEBUG_LOG_DISPATCHER = Some(DebugMessageDispatcher { handlers: Mutex::new(Vec::new()) });
        INITIALIZED.store(true, Ordering::Release);
    }

    pub fn register_handler<H: DebugMessageHandler + 'static>(handler: H) {
        unsafe {
            if INITIALIZED.load(Ordering::Acquire) {
                if let Some(dispatcher) = &DEBUG_LOG_DISPATCHER {
                    if let Ok(mut handlers) = dispatcher.handlers.lock() {
                        handlers.push(Box::new(handler));
                    }
                }
            }
        }
    }

    pub fn dispatch(message: &DebugMessage) {
        unsafe {
            if INITIALIZED.load(Ordering::Acquire) {
                if let Some(dispatcher) = &DEBUG_LOG_DISPATCHER {
                    if let Ok(handlers) = dispatcher.handlers.lock() {
                        for handler in &*handlers {
                            handler.handle_message(message);
                        }
                    }
                }
            }
        }
    }

    pub fn callback() -> Option<extern "system" fn(GLenum, GLenum, GLuint, GLenum, GLsizei, *const GLchar, *mut c_void)> {
        unsafe {
            if INITIALIZED.load(Ordering::Acquire) {
                Some(debug_callback)
            } else {
                None
            }
        }
    }
}

pub struct DebugMessagePrintlnHandler;

impl DebugMessageHandler for DebugMessagePrintlnHandler {
    fn handle_message(&self, message: &DebugMessage) {
        println!("OPENGL MESSAGE: {:?}, severity {:?}: {}", message.source, message.severity, message.text);
    }
}

extern "system" fn debug_callback(
    source: GLenum,
    _error_type: GLenum,
    _error_id: GLuint,
    severity: GLenum,
    _length: GLsizei,
    message: *const GLchar,
    _user_param: *mut c_void,
) {
    let text = unsafe {
        CStr::from_ptr(message)
    }.to_string_lossy();
    let source = DebugMessageSource::from_gl_enum(source);
    let severity = DebugMessageSeverity::from_gl_enum(severity);

    let message = DebugMessage {
        text,
        severity,
        source,
    };

    DebugMessageDispatcher::dispatch(&message);
}
