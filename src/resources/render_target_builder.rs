use crate::gl;
use crate::gl::Gl;
use crate::gl::types::*;
use crate::renderer::RenderTargetRef;
use crate::renderer::TextureRef;
use crate::{Renderer, material::Material, result::{ResourceResult, ResourceError}};

use super::render_target::RenderTarget;
use super::render_target::RenderTargetDepth;
use super::render_target::RenderTargetStencil;

pub struct RenderTargetBuilder<'r, Mat: Material> {
    renderer: &'r mut Renderer<Mat>,
    gl_id: GLuint,
    width: u32,
    height: u32,
    color_attachment: Option<TextureRef>,
    has_attachment: bool,
}

impl<'r, Mat: Material> RenderTargetBuilder<'r, Mat> {
    pub(crate) fn new(renderer: &mut Renderer<Mat>) -> ResourceResult<RenderTargetBuilder<Mat>> {
        let gl = &renderer.gl;

        let gl_id = unsafe {
            let mut fbo = 0;
            gl.GenFramebuffers(1, &mut fbo);
            if fbo == 0 {
                return Err(ResourceError::creation_failed());
            }
            gl_panic_on_error!(gl, "following framebuffer creation");

            gl.BindFramebuffer(gl::FRAMEBUFFER, fbo);

            fbo
        };

        Ok(RenderTargetBuilder { 
            renderer,
            gl_id,
            width: 0,
            height: 0,
            color_attachment: None,
            has_attachment: false,
        })
    }

    pub fn declare(&mut self, width: u32, height: u32) {
        self.width = width;
        self.height = height;
    }

    // TODO: attach_fragment_output - generates renderbuffer object for write-only FBO

    pub fn attach_fragment_output_texture(&mut self, index: u32, texture: &TextureRef) -> ResourceResult<()> {
        if index != 0 {
            unimplemented!("multiple color attachments not implemented - index must be 0");
        }

        unsafe {
            self.renderer.gl.FramebufferTexture2D(
                gl::FRAMEBUFFER, 
                gl::COLOR_ATTACHMENT0 + index,
                gl::TEXTURE_2D, 
                self.renderer.texture(texture).unwrap().gl_id, 
                0,
            );
            gl_panic_on_error!(&self.renderer.gl, "after attaching color attachment");
            self.has_attachment = true;
            self.color_attachment = Some(*texture);
        }
        Ok(())
    }

    pub fn attach_depth_stencil(
        &mut self,
        depth: Option<RenderTargetDepth>,
        stencil: Option<RenderTargetStencil>,
    ) -> ResourceResult<()> {
        if depth.is_none() && stencil.is_none() {
            return Err(ResourceError::invalid_argument("must specify at least depth or stencil"));
        }
        let mut rt1 = determine_attachment(&depth, &stencil);
        let mut rt2 = None;
        if rt1.is_none() {
            rt1 = determine_attachment(&depth, &None);
            rt2 = determine_attachment(&None, &stencil);
        }
        if rt1.is_none() && rt2.is_none() {
            return Err(ResourceError::invalid_argument(format!("Xenovisor does not support attachments for depth {:?} and stencil {:?}", depth, stencil)))
        }

        if let Some((format, attachment)) = rt1 {
            let rb = render_buffer(&self.renderer.gl, format, self.width, self.height)?;
            unsafe {
                self.renderer.gl.FramebufferRenderbuffer(
                    gl::FRAMEBUFFER, 
                    attachment, 
                    gl::RENDERBUFFER, 
                    rb
                );
                self.has_attachment = true;
            }

            gl_panic_on_error!(&self.renderer.gl, "after attaching depth/stencil or depth attachment");
        }

        if let Some((format, attachment)) = rt2 {
            let rb = render_buffer(&self.renderer.gl, format, self.width, self.height)?;
            unsafe {
                self.renderer.gl.FramebufferRenderbuffer(
                    gl::FRAMEBUFFER, 
                    attachment, 
                    gl::RENDERBUFFER, 
                    rb
                );
            }
            self.has_attachment = true;
            gl_panic_on_error!(&self.renderer.gl, "after attaching depth/stencil or stencil attachment");
        }

        
        Ok(())
    }

    // TODO: attach_depth_stencil_textures - attach using texture objects for readback

    pub fn build(self) -> ResourceResult<RenderTargetRef> {
        let renderer = self.renderer;
        let gl = &renderer.gl;

        let (width, height) = (self.width, self.height);
        assert!(width < i32::max_value() as u32);
        assert!(height < i32::max_value() as u32);

        if !self.has_attachment {
            return Err(ResourceError::invalid_argument("render target must have at least 1 attachment"));
        }
        
        if width == 0 || height == 0 {
            unsafe { gl.BindFramebuffer(gl::FRAMEBUFFER, 0) };
            return Err(ResourceError::invalid_argument(format!("width x height of framebuffer must be greater than zero - received {}x{}. Did you call declare?", width, height)))
        }

        let rt = unsafe {
            gl.BindFramebuffer(gl::FRAMEBUFFER, 0);

            // check completness
            if gl.CheckFramebufferStatus(gl::FRAMEBUFFER) != gl::FRAMEBUFFER_COMPLETE {
                return Err(ResourceError::incomplete("framebuffer"));
            }
            let gl_id = self.gl_id;
            let color_attachment = self.color_attachment;

            RenderTarget {
                gl_id,
                color_attachment,
                // width,
                // height,
            }
        };

        Ok(renderer.cache.alloc_render_target(rt))
    }
}

fn determine_attachment(
    depth: &Option<RenderTargetDepth>,
    stencil: &Option<RenderTargetStencil>,
) -> Option<(GLuint, GLuint)> {
    Some(match (depth, stencil) {
        (Some(RenderTargetDepth::F32), Some(RenderTargetStencil::U8)) => {
            (gl::DEPTH32F_STENCIL8, gl::DEPTH_STENCIL_ATTACHMENT)
        },
        (Some(RenderTargetDepth::U24), Some(RenderTargetStencil::U8)) => {
            (gl::DEPTH24_STENCIL8, gl::DEPTH_STENCIL_ATTACHMENT)
        },
        (Some(RenderTargetDepth::U16), Some(RenderTargetStencil::U8)) => {
            return None;
        },
        (Some(depth), None) => {
            let kind = match depth {
                RenderTargetDepth::F32 => gl::DEPTH_COMPONENT32F,
                RenderTargetDepth::U24 => gl::DEPTH_COMPONENT24,
                RenderTargetDepth::U16 => gl::DEPTH_COMPONENT16,
            };
            (kind, gl::DEPTH_ATTACHMENT)
        },
        (None, Some(RenderTargetStencil::U8)) => {
            (gl::STENCIL_INDEX8, gl::STENCIL_ATTACHMENT)
        },
        (None, None) => {
            return None;
        }
    })
}

fn render_buffer(gl: &Gl, format: GLuint, width: u32, height: u32) -> ResourceResult<GLuint> {
    let gl_id = unsafe {
        let mut rbo = 0;
        gl.GenRenderbuffers(1, &mut rbo);
        if rbo == 0 {
            return Err(ResourceError::creation_failed());
        }
        rbo
    };

    unsafe {
        gl.BindRenderbuffer(gl::RENDERBUFFER, gl_id);
        gl.RenderbufferStorage(gl::RENDERBUFFER, format, width as _, height as _);
        gl.BindRenderbuffer(gl::RENDERBUFFER, 0);
    }

    gl_panic_on_error!(gl, "after creating render buffer object");

    Ok(gl_id)
}