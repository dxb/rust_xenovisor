// Rust will insert this: #version 330 core

// TODO: Most of this isn't tested yet!

layout(location = 0) in vec3 pos;

uniform mat4 u_projection;
uniform mat4 u_camera;
uniform mat4 u_object;

#if defined(SurfaceNormals_PerPixel) || defined(SurfaceColor_Textured)
    layout(location = 4) in vec2 uv0;
    out vec2 v_uv0;

    void pass_uvs() {
        v_uv0 = uv0;
    }
#else
    void pass_uvs() { }
#endif

// Surface normals
#if defined(SurfaceNormals_PerPixel) || defined(SurfaceNormals_PerVertex)
    layout(location = 1) in vec3 normal;
    out vec3 v_normal;

    // TODO: Compute this guy outside the shader!
    // uniform mat3 u_object_normals;

    #if defined(SurfaceNormals_PerPixel)
        layout(location = 2) in vec3 tangent;

        out vec3 v_tangent;

        void pass_normals() {
            mat3 normal_matrix = mat3(transpose(inverse(u_object)));
            v_normal = normal_matrix * normal;
            v_tangent = normal_matrix * tangent;
        }
    #elif defined(SurfaceNormals_PerVertex)
        void pass_normals() {
            mat3 normal_matrix = mat3(transpose(inverse(u_object)));
            v_normal = normal_matrix * normal;
        }
    #endif
#else
    void pass_normals() { }
#endif

#if defined(Vertex_Colors)
    layout(location = 3) in vec4 color;

    out vec4 v_color;

    void pass_colors() {
        v_color = color;
    }
#else
    void pass_colors() { }
#endif

void main(void) {
    gl_Position = u_projection * u_camera * u_object * vec4(pos, 1.0);

    pass_normals();
    pass_colors();
    pass_uvs();
}