use crate::texture::*;
use crate::material::Material;
use crate::renderer::{Renderer, SamplerRef, TextureRef};

use crate::gl;

pub(crate) struct NewTextureCache {
    kind: Option<TextureKind>,
    internal_format: Option<TextureFormat>,
    sampler: Option<SamplerRef>,
    width: usize,
    height: usize,
    lods: Vec<ExtractMethod>,
}

pub struct TextureBuilder<'a, Mat: Material> { pub(crate) renderer: &'a mut Renderer<Mat> }

enum ExtractMethod {
    Buffer(ExtractBuffer),
}

struct ExtractBuffer {
    buffer: *const u8,
    buffer_byte_len: usize,
    upload_format: TextureUploadFormat,
}

impl NewTextureCache {
    pub(crate) fn new() -> Self {
        NewTextureCache {
            kind: None,
            internal_format: None,
            sampler: None,
            width: 0,
            height: 0,
            lods: Vec::new(),
        }
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.internal_format.is_none()
            && self.sampler.is_none()
            && self.width == 0
            && self.height == 0
            && self.lods.len() == 0
    }

    /// # Safety
    ///
    /// This function is unsafe as it may extract texture data from a raw pointer. The caller must
    /// guarantee that each mipmap level (aka lod) is sourced responsibly.
    ///
    /// # Panics
    ///
    /// Panics if the user failed to fully initialize a texture. Also panics if able to verify that
    /// an access is unsafe (e.g. a backing source buffer is not the expected size).
    unsafe fn new_texture<M: Material>(renderer: &mut Renderer<M>) -> Texture {
        let new_texture = &mut renderer.new_texture;
        let gl = &renderer.gl;

        // TODO: (Should) use immutable format storage instead (I think)
        // TODO: Check in one spot that everything is initialized properly

        let gl_id = {
            let mut tex = 0;
            gl.GenTextures(1, &mut tex);
            if tex == 0 {
                panic!(dbpi!("Failed to create a new texture"));
            }
            tex
        };
        let kind = new_texture.kind.expect(dbpi!("You must call declare in `create_texture`!"));
        match kind {
            TextureKind::Tex2d => {
                gl.BindTexture(gl::TEXTURE_2D, gl_id);
                gl.TexParameteri(
                    gl::TEXTURE_2D,
                    gl::TEXTURE_MAX_LEVEL,
                    ::std::cmp::max(new_texture.lods.len() - 1, 0) as _
                );
                for (level, extractor) in new_texture.lods.iter().enumerate() {
                    match extractor {
                        ExtractMethod::Buffer(extractor) => {
                            let (width, height) = (
                                (new_texture.width as f64 / ((1 << level) as f64))
                                    .floor() as usize,
                                (new_texture.height as f64 / ((1 << level) as f64))
                                    .floor() as usize,
                            );
                            let expected_size = width * height
                                * extractor.upload_format.pixel_size_bytes();
                            let size = extractor.buffer_byte_len;
                            assert_eq!(
                                size,
                                expected_size,
                                concat!(
                                    "Texture buffer size (left) must match the expected size",
                                    " (right) based on format",
                                ),
                            );
                            gl.TexImage2D(
                                gl::TEXTURE_2D,
                                level as _,
                                new_texture.internal_format.unwrap().as_gl_enum() as _,
                                width as _,
                                height as _,
                                0,
                                extractor.upload_format.as_gl_enum(),
                                gl::UNSIGNED_BYTE,
                                extractor.buffer as *const _,
                            );
                        }
                    }

                }
                gl.BindTexture(gl::TEXTURE_2D, 0);
            },
            TextureKind::Tex2dArray | TextureKind::TexCubeMap => {
                unimplemented!("use TextureBuilder2");
            },
        }
        Texture {
            height: new_texture.height,
            width: new_texture.width,
            sampler: new_texture.sampler.unwrap(),
            kind,
            gl_id,
        }
    }

    fn clear(&mut self) {
        self.internal_format = None;
        self.width = 0;
        self.height = 0;
        self.lods.clear();
        self.kind = None;
        self.sampler = None;
    }
}
impl<'a, M: Material> TextureBuilder<'a, M> {
    pub fn declare(
        &mut self,
        kind: TextureKind,
        internal_format: TextureFormat,
        width: usize,
        height: usize,
        sampler: SamplerRef,
    ) {
        self.renderer.new_texture.height = height;
        self.renderer.new_texture.width = width;
        self.renderer.new_texture.internal_format = Some(internal_format);
        self.renderer.new_texture.kind = Some(kind);
        self.renderer.new_texture.sampler = Some(sampler);
    }

    pub unsafe fn mipmap_from_buffer<T>(
        &mut self,
        buffer: *const T,
        buffer_byte_len: usize,
        upload_format: TextureUploadFormat,
    ) {
        self.renderer.new_texture.lods.push(ExtractMethod::Buffer(
            ExtractBuffer {
                buffer: buffer as *const _,
                buffer_byte_len,
                upload_format,
            }
        ));
    }

    pub(crate) fn finalize(mut self) -> TextureRef {
        let tex = unsafe { NewTextureCache::new_texture(&mut self.renderer) };
        self.renderer.new_texture.clear();
        self.renderer.cache.alloc_texture(tex)
    }
}
