use std::io::Cursor;
use std::io::Read;
use std::io;
use std::mem;
use std::cmp;
use std::slice;
use std::usize;

use crate::mesh::IndexBufferTypeInstance;
use crate::mesh::Mesh;
use crate::mesh::MeshPart;
use crate::renderer::MaterialRef;
use crate::renderer::MeshRef;
use crate::rgl;
use crate::{Renderer, gl};
use crate::material::Material;
use crate::mesh::{BufferFormat, AttributeFormat, Sva, SvaType};
use crate::gl::types::*;

pub struct MeshBuilder2<'r, M: Material> {
    vao: GLuint,
    ibo: Option<(GLuint, SvaType, isize)>,
    buffers: Vec<GLuint>,
    buffer_sizes: Vec<usize>,
    attribute_format: AttributeFormat,
    material: Option<MaterialRef>,
    parts: Vec<Option<MeshPart>>,
    renderer: &'r mut Renderer<M>,
}

impl<'r, M: Material> MeshBuilder2<'r, M> {
    pub(crate) fn new(renderer: &mut Renderer<M>) -> MeshBuilder2<M> {
        let attribute_format = AttributeFormat {
            colors: None,
            normals: None,
            pos: None,
            uvs: None,
        };

        MeshBuilder2 {
            vao: new_vao(renderer),
            ibo: None,
            buffers: Vec::new(),
            buffer_sizes: Vec::new(),
            attribute_format,
            material: None,
            parts: Vec::new(),
            renderer,
        }
    }

    fn clear_part_in_progress(&mut self) {
        self.vao = new_vao(&mut self.renderer);
        self.ibo = None;
        self.buffers.clear();
        self.buffer_sizes.clear();
        self.attribute_format = AttributeFormat {
            colors: None,
            normals: None,
            pos: None,
            uvs: None,
        };
        self.material = None;
    }

    pub fn use_buffer_from_reader<R: Read>(&mut self, reader: &mut R, size: usize) -> Result<(), io::Error> {
        let buffer: Vec<u8> = Vec::with_capacity(size);
        let mut cursor = Cursor::new(buffer);
        let read = io::copy(reader, &mut cursor)?;
        let buffer = cursor.into_inner();
        self.use_buffer(&buffer);
        assert_eq!(read, size as u64, "read more bytes than expected");
        Ok(())
    }

    // TODO: is this fully safe to support T?
    pub fn use_buffer<T>(&mut self, buffer: &[T]) {
        unsafe {
            let gl = &self.renderer.gl;

            let vbo = rgl::create_buffer(gl)
                .expect(dbpi!("Failed to create VBO"));

            gl.BindBuffer(gl::ARRAY_BUFFER, vbo);

            let byte_length = mem::size_of_val(buffer);

            gl.BufferData(
                gl::ARRAY_BUFFER,
                byte_length as _,
                buffer.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            gl_panic_on_error!(gl, "following vbo upload");

            self.buffers.push(vbo);
            self.buffer_sizes.push(byte_length);
        }
    }

    pub fn use_index_buffer_from_reader<R: Read>(
        &mut self,
        reader: &mut R,
        size: usize,
        kind: SvaType,
        elements: isize,
    ) -> Result<(), io::Error> {
        let buffer: Vec<u8> = Vec::with_capacity(size);
        let mut cursor = Cursor::new(buffer);
        let read = io::copy(reader, &mut cursor)?;
        let buffer = cursor.into_inner();
        self.use_index_buffer_untyped(&buffer, kind, elements);
        assert_eq!(read, size as u64, "read more bytes than expected");
        Ok(())
    }

    pub fn use_index_buffer<T: IndexBufferTypeInstance>(
        &mut self,
        buffer: &[T],
    ) {
        let elements = buffer.len();
        unsafe {
            let buffer: &[u8] = slice::from_raw_parts(buffer.as_ptr() as *const u8, mem::size_of_val(buffer));
            let kind = T::sva_type();
            self.use_index_buffer_untyped(
                buffer,
                kind,
                elements as _,
            )
        }
    }

    pub fn use_index_buffer_untyped(
        &mut self,
        buffer: &[u8],
        kind: SvaType,
        elements: isize,
    ) {
        unsafe {
            let gl = &self.renderer.gl;

            let ibo = rgl::create_buffer(gl)
                .expect(dbpi!("Failed to create IBO"));

            let byte_length = mem::size_of_val(buffer);

            gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ibo);
            gl.BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                byte_length as _,
                buffer.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );
            gl.BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
            gl_panic_on_error!(gl, "following ibo specification");

            let anticipated_length = kind.size_bytes() * (elements as usize);
            assert_eq!(
                anticipated_length,
                byte_length,
                "Supplied index buffer length is different from anticipated length",
            );

            self.ibo = Some((ibo, kind, elements as _));
        }
    }

    /// Specify the attribute definition.
    ///
    /// If `buffer` is provided, this means the attribute refers to the `nth`
    /// buffer specified on this builder. Otherwise, this method will use the
    /// last buffer.
    ///
    /// # Panics
    ///
    /// Panics if called before any buffers are specified - the buffer that
    /// this attribute refers to must be specified before calling this method,
    /// using the `use_buffer` and `use_buffer_from_reader` methods.
    ///
    /// # Safety
    ///
    /// The validity of the underlying data is not checked - you can violate
    /// memory safety by providing bogus formats that refer to data that isn't
    /// valid for the specified type.
    pub unsafe fn use_attribute(
        &mut self,
        buffer: Option<usize>,
        attr: Sva,
        format: &BufferFormat,
    ) {
        let attr_format = match attr {
            Sva::Position => &mut self.attribute_format.pos,
            Sva::Normal => &mut self.attribute_format.normals,
            Sva::Color => &mut self.attribute_format.colors,
            Sva::Uv => &mut self.attribute_format.uvs,
        };

        // Check if this attribute has already been declared
        if attr_format.is_some() {
            panic!("declared {:?} attribute more than once!", &attr);
        }

        let buffer = buffer.unwrap_or(self.buffers.len() - 1);
        *attr_format = Some((buffer, *format));

        let gl = &self.renderer.gl;
        // bind the right buffer
        let vbo = self.buffers[buffer];
        gl.BindBuffer(gl::ARRAY_BUFFER, vbo);
        // upload the format
        let array_index = attr.array_index();
        gl.EnableVertexAttribArray(array_index);
        gl.VertexAttribPointer(
            array_index,
            format.components as _,
            format.kind.as_glenum(),
            format.normalized as _,
            format.vertex_attrib_pointer_stride() as _,
            format.vertex_attrib_pointer_offset() as *const _,
        );

        gl_panic_on_error!(gl, "following vbo specification");
    }

    pub fn material(&mut self, m: MaterialRef) {
        if self.material.is_some() {
            panic!("A part can only have one material!");
        }
        self.material = Some(m);
    }

    /// Complete the mesh.
    /// 
    /// If your mesh contains multiple materials, you can create several parts
    /// (a.k.a primitives) for the mesh by calling `build_part` after specifying
    /// each part.
    /// 
    /// If your mesh contains a single material (or a single primitive), you can 
    /// call `build` directly.
    /// 
    /// # Panics
    /// 
    /// If your mesh has multiple parts, be sure to call `build_part` for all parts,
    /// including the last part. If you started declaring a part, and fail to call
    /// `build_part` when done, this will panic.
    pub fn build(mut self) -> MeshRef {
        if self.parts.len() == 0 {
            self.build_part();
        } else if self.buffers.len() > 0 {
            // the user called `build_part manually, but started building a second part
            // without finalizing it with build_part
            panic!("Cannot build mesh with an incomplete part");
        }

        let cache = &mut self.renderer.cache;
        let parts = cache.alloc_mesh_parts(&mut self.parts[..]);
        let mesh = cache.alloc_mesh(Mesh {
            parts,
        });

        mesh
    }

    pub fn build_part(&mut self) {
        unsafe {
            // cleanup
            self.renderer.gl.BindBuffer(gl::ARRAY_BUFFER, 0);
            self.renderer.gl.BindVertexArray(0);
        }

        // check buffer length is consistent
        let mut buffer_elements = None;
        for (buffer, format) in self.attribute_format.iter() {
            let byte_size = self.buffer_sizes[*buffer];
            let mut stride = format.vertex_attrib_pointer_stride();
            if stride == 0 {
                stride = format.element_size_bytes();
            }
            let expected_elements = byte_size / stride;
            let last_elements = buffer_elements.unwrap_or(usize::MAX);
            buffer_elements = Some(cmp::min(expected_elements, last_elements));
        }

        assert!(buffer_elements.is_some(), "no attributes were declared");
        let buffer_elements = buffer_elements.unwrap() as isize;

        // finalize
        let cache = &mut self.renderer.cache;
        let attribute_format = cache.alloc_format(self.attribute_format);
        let vao = self.vao;
        let vbos = cache.alloc_vbos(&self.buffers);
        let material = self.material.take()
            .expect(dbpi!("You must declare exactly one material per part"));
        cache.assert_material_attrib_compatibility(&material, &attribute_format);

        let mut elements = buffer_elements;
        let mut ibo = 0;
        let mut ibo_type = gl::UNSIGNED_SHORT;
        if let Some((specified_ibo, specified_ibo_type, ibo_elements)) = self.ibo {
            ibo = specified_ibo;
            ibo_type = specified_ibo_type.as_glenum();
            elements = ibo_elements;
        }

        let part = MeshPart {
            attribute_format,
            vbos,
            vao,
            ibo,
            ibo_type,
            elements,
            material,
        };
        
        self.parts.push(Some(part));
        self.clear_part_in_progress();
    }
}

fn new_vao<M: Material>(renderer: &mut Renderer<M>) -> GLuint {
    let vao = rgl::create_vao(&renderer.gl)
        .unwrap();
    unsafe {
        renderer.gl.BindVertexArray(vao);
    }
    vao
}