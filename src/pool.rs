use std::marker::PhantomData;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::borrow::Borrow;
use std::fmt;

/// Allocates storage for variable sized slices of V which have unique identifiers.
///
/// Each item may optionally be named ("aliased").
///
/// TODO: This isn't efficient in the slightest! We can make this pretty awesome, but it's basically
/// writing a special allocator in order to do that!
pub struct NamedSlicePool<K, T, G>
    where K: Hash + Eq,
        G: Default + Eq + Copy,
{
    names: HashMap<K, SlicePoolRef<T, G>>,
    /// References a backing array.
    ///
    /// TODO: For now, usize corresponds to index and we won't re-use any indices (so we don't
    /// need to actually use generations)!
    pool: HashMap<usize, Box<[T]>>,
    next_id: usize,
    // TODO: Use generations
    _marker: PhantomData<G>,
}

// TODO: Should NamedSlicePool wrap SlicePool?

pub type SlicePool<T, G> = NamedSlicePool<(), T, G>;

// We cannot derive anything as derives will put bounds on T even though we don't actually own a T.
// We also don't want to use something like &T in the phantom data as then all users must provide a
// lifetime and the whole point of indices was to free ourselves from lifetimes (kind of)!
// (TODO: Derive debug manually)
#[derive(Debug)]
pub struct SlicePoolRef<T, G>
    where G: Default + Eq + Copy
{
    index: usize,
    // TODO: Support multiple generations!
    _generation: G,
    _marker: PhantomData<T>,
}

impl<K, T, G> NamedSlicePool<K, T, G>
    where K: Hash + Eq,
        G: Default + Eq + Copy,
        T: Copy
{
    pub fn alloc_from_copy(&mut self, items: &[T]) -> SlicePoolRef<T, G> {
        let index = self.next_id;
        self.next_id += 1;
        // OPTIMIZATION: set_len and copy_from_slice perhaps?
        let mut slice = Vec::with_capacity(items.len());
        slice.extend_from_slice(items);
        self.pool.insert(index, slice.into_boxed_slice());
        SlicePoolRef {
            index,
            _generation: Default::default(),
            _marker: PhantomData,
        }
    }
}

impl<K, T, G> NamedSlicePool<K, T, G>
    where K: Hash + Eq,
        G: Default + Eq + Copy,
{
    pub fn new() -> Self {
        NamedSlicePool {
            names: HashMap::new(),
            pool: HashMap::new(),
            next_id: Default::default(),
            _marker: PhantomData,
        }
    }

    pub fn alloc_from_slice(&mut self, items: &mut[Option<T>]) -> SlicePoolRef<T, G> {
        let index = self.next_id;
        self.next_id += 1;
        let mut slice = Vec::with_capacity(items.len());
        // Is using iter.collect any more or less efficient?
        for i in items {
            slice.push(i.take().expect("You must populate the entire slice"));
        }
        self.pool.insert(index, slice.into_boxed_slice());
        SlicePoolRef {
            index,
            _generation: Default::default(),
            _marker: PhantomData,
        }
    }

    pub fn realloc_and_extend(&mut self, i: &SlicePoolRef<T, G>, item: T) -> Option<()> {
        // this is so dumb we really need to rewrite the slice pool ;^)
        let items = self.pool.remove(&i.index)?;
        let mut ext = Vec::from(items);
        ext.push(item);
        self.pool.insert(i.index, ext.into());
        Some(())
    }

    pub fn alias(&mut self, item: &SlicePoolRef<T, G>, name: K) {
        debug_assert!(self.get(item).is_some(), "Tried to alias a nonexistent item!");
        self.names.insert(name, *item);
    }

    /// Remove an element from the pool, returning None if it does not exist.
    ///
    /// This *will not remove any aliases*. Furthermore, using those aliases will cause a panic!
    /// See free_with_alias and clear_aliases.
    pub fn free(&mut self, i: &SlicePoolRef<T, G>) -> Option<()> {
        // We cannot return the boxed slice because eventually our implementation will (hopefully)
        // not use box.
        self.pool.remove(&i.index)
            .map(|_| ())
    }

    /// Remove an element from the pool, returning None if it does not exist with that alias.
    pub fn free_with_alias(&mut self, k: &K) -> Option<()> {
        let index = self.names.remove(k)?.index;
        self.pool.remove(&index)
            .map(|_| ())
    }

    /// Dealias/unalias some object; remove all aliases/names (if any).
    pub fn clear_aliases(&mut self, i: &SlicePoolRef<T, G>) {
        // retain every alias that *does not* map to index i
        self.names.retain(|_, v| v != i);
    }

    pub fn get(&self, i: &SlicePoolRef<T, G>) -> Option<&[T]> {
        self.pool.get(&i.index)
            .map(|b| -> &[T] { &*b })
    }

    pub fn get_mut(&mut self, i: &SlicePoolRef<T, G>) -> Option<&mut [T]> {
        self.pool.get_mut(&i.index)
            .map(|b| -> &mut [T] { &mut *b })
    }

    pub fn get_by_alias<Q: ?Sized>(&self, n: &Q) -> Option<&[T]>
        where K: Borrow<Q>,
            Q: Hash + Eq,
    {
        self.names.get(n)
            .and_then(|i| self.get(i))
    }
}

impl<T,G> Clone for SlicePoolRef<T, G>
    where G: Default + Eq + Copy,
{
    fn clone(&self) -> Self {
        SlicePoolRef {
            index: self.index,
            _generation: self._generation,
            _marker: PhantomData,
        }
    }
}

impl<T, G: Default + Eq + Copy> Copy for SlicePoolRef<T, G> {}

impl<T, G> PartialEq for SlicePoolRef<T, G>
    where G: Default + Eq + Copy,
{
    fn eq(&self, other: &SlicePoolRef<T, G>) -> bool {
        self.index == other.index
            && self._generation == other._generation
    }
}

impl<T, G: Default + Eq + Copy> Eq for SlicePoolRef<T, G> {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_creation() {
        let _: SlicePool<u32, u32> = SlicePool::new();
    }

    fn create_basic(data: &mut [u32]) -> (SlicePool<u32, u32>, SlicePoolRef<u32, u32>) {
        let mut pool: SlicePool<u32, u32> = SlicePool::new();
        // Doesn't need to be efficient! :)
        let mut data: Vec<_> = data.iter().map(|n| Some(*n)).collect();
        let buf0 = pool.alloc_from_slice(data.as_mut_slice());
        (pool, buf0)
    }

    #[test]
    fn test_alloc() {
        let (_, _) = create_basic(&mut [3, 4, 5]);
    }

    #[test]
    fn test_fetch() {
        let (pool, item) = create_basic(&mut [3, 4, 5]);
        let slice = pool.get(&item).unwrap();
        assert_eq!(slice, [3, 4, 5]);
    }

    #[test]
    fn test_modify() {
        let (mut pool, item) = create_basic(&mut [3, 4, 5]);
        {
            // get a slice mutably
            let slice = pool.get_mut(&item).unwrap();
            slice[1] = 8;
        }
        {
            // get the slice again and check that the result is what we expect
            let slice = pool.get(&item).unwrap();
            assert_eq!(slice, [3, 8, 5]);
        }
    }

    #[test]
    fn test_free() {
        let (mut pool, item) = create_basic(&mut [3, 4, 5]);
        pool.free(&item);
    }

    #[test]
    #[should_panic]
    fn test_use_after_free() {
        let (mut pool, item) = create_basic(&mut [3, 4, 5]);
        pool.free(&item);
        pool.get(&item)
            .expect("This should panic because we freed `item`!");
    }

    #[test]
    fn test_indices_are_unique() {
        let (mut pool, item) = create_basic(&mut [3, 4, 5]);
        pool.free(&item);
        let new_item = pool.alloc_from_slice(&mut [Some(2), Some(5)]);
        assert_ne!(item, new_item);
        assert!(pool.get(&item).is_none());
        assert!(pool.get(&new_item).is_some());
    }

    #[test]
    fn test_alias() {
        let mut pool: NamedSlicePool<_, _, u32> = NamedSlicePool::new();
        let item = pool.alloc_from_slice(&mut [Some(3), Some(5), Some(6)]);
        pool.alias(&item, "Hello World".to_string());
        assert_eq!(pool.get_by_alias("Hello World").unwrap(), [3, 5, 6]);
    }
}

pub struct Pool<T> {
    buffer: Vec<Option<T>>,
}

pub struct PoolRef<T>(usize, PhantomData<T>);

impl<T> Pool<T> {
    pub fn new() -> Self {
        Pool {
            buffer: Vec::new(),
        }
    }

    pub fn alloc(&mut self, item: T) -> PoolRef<T> {
        let index = self.buffer.len();
        self.buffer.push(Some(item));
        PoolRef(index, PhantomData)
    }

    pub fn free(&mut self, index: &PoolRef<T>) -> Option<T> {
        self.buffer.get_mut(index.0)
            .and_then(|item| item.take())
    }

    pub fn get(&self, index: &PoolRef<T>) -> Option<&T> {
        self.buffer.get(index.0)
            .and_then(|element: &Option<T>| element.as_ref())
    }

    pub fn get_mut(&mut self, index: &PoolRef<T>) -> Option<&mut T> {
        self.buffer.get_mut(index.0)
            .and_then(|element: &mut Option<T>| element.as_mut())
    }

    pub fn iter(&self) -> impl Iterator<Item=&T> {
        self.into_iter()
    }

    pub fn find_ref<P: FnMut(&T) -> bool>(&self, mut predicate: P) -> Option<PoolRef<T>> {
        self.iter()
            .enumerate()
            .find(|(_, item)| {
                predicate(*item)
            })
            .map(|(index, _)| PoolRef(index, PhantomData))
    }
}

impl<T> PartialEq for PoolRef<T> {
    fn eq(&self, other: &PoolRef<T>) -> bool {
        self.0 == other.0
    }
}

impl<T> Eq for PoolRef<T> {}

impl<T> Hash for PoolRef<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl<T> Clone for PoolRef<T> {
    fn clone(&self) -> Self {
        PoolRef(self.0, PhantomData)
    }
}

impl<T> Copy for PoolRef<T> {}

impl<T> fmt::Debug for PoolRef<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "PoolRef {{ index: {} }}", self.0)
    }
}

impl<'a, T> IntoIterator for &'a Pool<T> {
    type Item = &'a T;
    type IntoIter = ::std::iter::FilterMap<::std::slice::Iter<'a, Option<T>>, fn(&'a Option<T>) -> Option<&'a T>>;

    fn into_iter(self) -> Self::IntoIter {
        self.buffer.iter()
            .filter_map(|i| {
                i.as_ref()
            })
    }
}

impl<'a, T> IntoIterator for &'a mut Pool<T> {
    type Item = &'a mut T;
    type IntoIter = ::std::iter::FilterMap<::std::slice::IterMut<'a, Option<T>>, fn(&'a mut Option<T>) -> Option<&'a mut T>>;

    fn into_iter(self) -> Self::IntoIter {
        self.buffer.iter_mut()
            .filter_map(|i| {
                i.as_mut()
            })
    }
}

#[derive(Debug, Clone)]
pub struct OpaqueRef(PoolRef<()>);

impl<T> From<PoolRef<T>> for OpaqueRef {
    fn from(p: PoolRef<T>) -> Self {
        OpaqueRef(PoolRef(p.0, PhantomData))
    }
}

impl OpaqueRef {
    pub fn trust_ref<T>(&self) -> PoolRef<T> {
        PoolRef((self.0).0, PhantomData)
    }
}
