use crate::gl::types::*;

use crate::renderer::TextureRef;

/// A framebuffer is a read-only texture resource which can be rendered to.
pub struct RenderTarget {
    pub(crate) gl_id: GLuint,
    pub(crate) color_attachment: Option<TextureRef>,
    // pub(crate) width: u32,
    // pub(crate) height: u32,
}

#[derive(Debug, Clone)]
pub enum RenderTargetDepth {
    F32,
    // Note: Not sure what to call the non floating point options...
    U24,
    U16, 
}

#[derive(Debug, Clone)]
pub enum RenderTargetStencil {
    U8,
}