//! # Xenovisor Core
//!
//! `xenovisor-core` provides a higher level interface for rendering and managing GPU resources than
//! a native graphics API. It provides tools so that you can work with meshes and materials instead
//! of shaders and buffers, although `xenovisor-core` still assumes you know certain low level
//! details in order to still give you full control over your data layouts.
//!
//! Future libraries in the Xenovisor family will provide even higher level abstractions and
//! pre-packaged Material tooling like you would find in a complete engine package.
//!
//! # Overview
//!
//! To use xenovisor-core, you need to create a `Renderer` and supply it with a `Material` type.
//!
//! To implement `Material` for your type, you need to define:
//!  - `MaterialData`, per material data that gets mapped to shader uniforms
//!  - `SceneData`, per render-call data that gets mapped to shader uniforms
//!  - `MeshData`, per mesh data that gets mapped to shader uniforms
//!  - `ProgramGenerator`, which can generate GLSL shaders

pub mod gl {
    include!(concat!(env!("OUT_DIR"), "/gl_bindings.rs"));
}

/// Debugging procedure info, outputs file and location so you don't have to read panic backtraces.
///
/// This comes in pretty handy, especially when you use it with `expect`, so you know immediately
/// where the panic "semantically" originates, instead of where it is invoked, e.g. inside result.
///
/// This isn't exported since the name is so unintuitive, but it's certainly handy copy pasta!
///
/// TODO: Allow turning this off
macro_rules! dbpi {
    ($msg:expr) => {
        concat!($msg, "; ", file!(), ":", line!())
    };
    () => {
        concat!(file!(), ":", line!())
    };
}


#[macro_use]
pub mod rgl;

pub mod debug;
pub mod result;
pub mod pool;
pub mod mesh;
pub mod texture;
pub mod resources;
pub mod material;
pub mod legacy;
pub mod texture_loader;
pub mod texture_builder;
pub mod loader;
pub mod mesh_builder;
pub mod renderer;
pub mod camera;
pub mod blending;

pub use crate::renderer::Renderer;
