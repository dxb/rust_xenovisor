//! Legacy Shader Material for the Diffuse/Specular rendering of yesteryear.
#![allow(dead_code)]

use crate::gl;
use crate::gl::types::*;

use crate::renderer::TextureRef;
use crate::material::{
    Material,
    UniformSet,
    GenerateProgram,
    ProgramCompiler,
    ShaderGenerator,
};

use std::fmt::Write;
use std::borrow::Cow;

/// These are uniforms that come from the scene, primarily lighting related.
pub struct SceneUniforms {
    pub ambient_light: [f32; 3],
    pub view: [f32; 16],
    pub projection: [f32; 16],
    pub directional_lights: Vec<DirectionalLight>,
}

pub struct DirectionalLight {
    pub intensity: [f32; 3],
    pub dir: [f32; 3],
}

#[derive(Debug, Copy, Clone)]
pub struct LegacyMaterial {
    pub diffuse: LegacyDiffuse,
    pub use_vertex_colors: bool,
}

#[derive(Debug, Copy, Clone)]
pub enum LegacyDiffuse {
    Color([f32; 4]),
    Texture(TextureRef),
}

impl Material for LegacyMaterial {
    type MeshData = [f32; 16];
    type SceneData = SceneUniforms;
    type MaterialData = Self;

    type ProgramGenerator = LegacyShaderGenerator;

    fn program_generator(&self) -> Cow<Self::ProgramGenerator> {
        Cow::Owned(determine_capabilities(self))
    }

    fn material_uniforms(&self) -> Cow<Self::MaterialData> {
        Cow::Borrowed(self)
    }
}

unsafe impl UniformSet for SceneUniforms {
    fn count() -> usize {
        6
    }

    fn name(field_index: usize) -> &'static str {
        match field_index {
            0 => "u_ambient_light",
            1 => "u_camera",
            2 => "u_projection",
            3 => "u_directional_light_count",
            4 => "u_directional_light_dir[0]",
            5 => "u_directional_light_intensity[0]",
            _ => panic!()
        }
    }

    fn glsl_type(field_index: usize) -> GLenum {
        match field_index {
            0 => gl::FLOAT_VEC3,
            1 => gl::FLOAT_MAT4,
            2 => gl::FLOAT_MAT4,
            3 => gl::INT,
            4 => gl::FLOAT_VEC3,
            5 => gl::FLOAT_VEC3,
            _ => panic!()
        }
    }

    fn derive<F: FnOnce(*const u8)>(&self, f: F, field_index: usize) {
        match field_index {
            0 => f(self.ambient_light.as_ptr() as *const _),
            1 => f(self.view.as_ptr() as *const _),
            2 => f(self.projection.as_ptr() as *const _),
            3 => f(&self.directional_lights.len() as *const _ as *const _),
            4 => {
                if self.directional_lights.len() > 0 {
                    f(self.directional_lights[0].dir.as_ptr() as *const _)
                }
            },
            5 => {
                if self.directional_lights.len() > 0 {
                    f(self.directional_lights[0].intensity.as_ptr() as *const _)
                }
            },
            _ => panic!(),
        }
    }
}

unsafe impl UniformSet for LegacyMaterial {
    fn count() -> usize {
        3
    }

    fn name(field_index: usize) -> &'static str {
        match field_index {
            0 => "u_color",
            1 => "u_diffuse",
            2 => "u_normal_tex",
            _ => panic!(),
        }
    }

    fn glsl_type(field_index: usize) -> GLenum {
        match field_index {
            0 => gl::FLOAT_VEC4,
            1 | 2 => gl::INT,
            _ => panic!(),
        }
    }

    fn derive<F: FnOnce(*const u8)>(&self, f: F, field_index: usize) {
        let mut u_color = [1.0, 1.0, 1.0, 1.0];
        if let LegacyDiffuse::Color(color) = &self.diffuse {
            u_color = color.clone();
        }
        match field_index {
            0 => f(u_color.as_ptr() as *const _),
            1 => f(&0 as *const _ as *const _),
            2 => f(&-1 as *const _ as *const _),
            _ => panic!(),
        }
    }

    fn tex_count(&self) -> usize {
        match self.diffuse {
            LegacyDiffuse::Texture(_) => 1,
            _ => 0,
        }
    }

    fn tex_ref(&self, field_index: usize) -> (u32, &TextureRef) {
        if field_index == 0 {
            match &self.diffuse {
                LegacyDiffuse::Texture(tex) => (0, tex),
                _ => panic!(),
            }
        } else {
            panic!()
        }
    }
}

unsafe impl UniformSet for [f32; 16] {
    fn count() -> usize {
        1
    }

    fn name(field_index: usize) -> &'static str {
        match field_index {
            0 => "u_object",
            _ => panic!(),
        }
    }

    fn glsl_type(field_index: usize) -> GLenum {
        match field_index {
            0 => gl::FLOAT_MAT4,
            _ => panic!(),
        }
    }

    fn derive<F: FnOnce(*const u8)>(&self, f: F, field_index: usize) {
        match field_index {
            0 => f(self.as_ptr() as *const _),
            _ => panic!(),
        }
    }
}

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct LegacyShaderGenerator {
    shading: LegacyShadingMode,
    surface_normals: SurfaceNormals,
    surface_color: SurfaceColor,
    max_directional_lights: u32,
    vertex_colors: bool,
}

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
enum SurfaceNormals {
    /// Uses a camera facing normal
    Auto,
    /// Requires vertex normals
    PerVertex,
    /// Requires both vertex normals, tangents, AND normal map
    PerPixel,
}

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
enum SurfaceColor {
    /// Shader picks some lame ugly grey color
    Auto,
    Solid,
    /// Requires 2D UVs
    Textured,
}

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq, Hash)]
enum LegacyShadingMode {
    /// Surface color only
    Unlit,
    /// Surface color + ambient light + scene lighting
    Diffuse,
    /// All diffuse lighting above + specular
    DiffuseSpecular,
}

unsafe impl GenerateProgram for LegacyShaderGenerator {
    fn generate(&self, compiler: &mut ProgramCompiler) {
        let header = shader_header(self);

        compiler.vertex(ShaderGenerator {
            glsl_flavor: &header,
            glsl_decl: "",
            glsl: include_str!("shaders/legacy.vs"),
        });

        compiler.fragment(ShaderGenerator {
            glsl_flavor: &header,
            glsl_decl: "",
            glsl: include_str!("shaders/legacy.fs"),
        });
    }
}

pub fn determine_capabilities(
    mat: &LegacyMaterial,
) -> LegacyShaderGenerator {
    let surface_color = match mat.diffuse {
        LegacyDiffuse::Color(_) => SurfaceColor::Solid,
        LegacyDiffuse::Texture(_) => SurfaceColor::Textured,
    };
    let surface_normals = SurfaceNormals::PerVertex;
    let vertex_colors = mat.use_vertex_colors;
    LegacyShaderGenerator {
        shading: LegacyShadingMode::Diffuse,
        surface_color,
        surface_normals,
        vertex_colors,
        max_directional_lights: 2,
    }
}


pub fn shader_header(params: &LegacyShaderGenerator) -> String {
    if params.max_directional_lights != 2 {
        unimplemented!(); // TODO configurable lighting
    }
    let shading = match params.shading {
        LegacyShadingMode::Unlit => "Shading_Unlit",
        LegacyShadingMode::Diffuse => "Shading_Diffuse",
        LegacyShadingMode::DiffuseSpecular => unimplemented!(), // TODO specular
    };
    let surface_normals = match params.surface_normals {
        SurfaceNormals::Auto => "BLANK",
        SurfaceNormals::PerVertex => "SurfaceNormals_PerVertex",
        SurfaceNormals::PerPixel => "SurfaceNormals_PerPixel",
    };
    let surface_color = match params.surface_color {
        SurfaceColor::Auto => "BLANK",
        SurfaceColor::Solid => "SurfaceColor_Solid",
        SurfaceColor::Textured => "SurfaceColor_Textured",
    };
    let vertex_colors = if params.vertex_colors { "Vertex_Colors" } else { "BLANK" };
    let mut s = String::new();
    write!(&mut s, "#version 330 core\n").unwrap();
    write!(&mut s, "#define {}\n", shading).unwrap();
    write!(&mut s, "#define {}\n", surface_normals).unwrap();
    write!(&mut s, "#define {}\n", surface_color).unwrap();
    write!(&mut s, "#define {}\n", vertex_colors).unwrap();
    s
}
