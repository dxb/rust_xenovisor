use std::{error::Error, fmt::Display};

pub type ResourceResult<T> = std::result::Result<T, ResourceError>;

#[derive(Debug, Clone)]
pub struct ResourceError {
    kind: ResourceErrorKind,
}

impl ResourceError {
    pub fn creation_failed() -> ResourceError {
        ResourceError {
            kind: ResourceErrorKind::CreationFailed,
        }
    }

    pub fn incomplete<S1: Into<String>>(resource: S1) -> ResourceError {
        ResourceError {
            kind: ResourceErrorKind::Incomplete(resource.into(), None),
        }
    }

    pub fn invalid_argument<S1: Into<String>>(error: S1) -> ResourceError {
        ResourceError {
            kind: ResourceErrorKind::InvalidArgument(error.into()),
        }
    }

    pub fn incomplete_because<S1: Into<String>, S2: Into<String>>(resource: S1, reason: S2) -> ResourceError {
        ResourceError {
            kind: ResourceErrorKind::Incomplete(resource.into(), Some(reason.into())),
        }
    }
}

impl Error for ResourceError {}

impl Display for ResourceError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.kind {
            ResourceErrorKind::CreationFailed => write!(f, "Resource Creation Failed"),
            ResourceErrorKind::Incomplete(obj, reason) => {
                let reason = reason.as_ref().map(|s| s.as_str())
                    .unwrap_or("not specified");
                write!(f, "Resource Incomplete: {} ; due to: {}", obj, reason)
            },
            ResourceErrorKind::InvalidArgument(error) => {
                write!(f, "Invalid Resource Argument: {}", error)
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum ResourceErrorKind {
    CreationFailed,
    Incomplete(String, Option<String>),
    InvalidArgument(String),
}