Xenovisor is a work-in-progress backend-API agnostic renderer, meaning the API will not* expose the details of something like OpenGL, Vulkan, Metal, DirectX, etc.

Currently it only supports OpenGL as the current focus is on the external API rather than the internal architecture. The API is on a slow march towards being something decent - **currently the API is neither good nor complete.** (*) OpenGL is still exposed in a few places out of pure laziness/expedience in using it other projects. **This library is not recommended for use.**

Current status:
 - I enjoy using this in several projects (which are not very far along graphically)
 - It's certainly better than using OpenGL directly, and makes plenty of basic decisions for you
 - The material system is bad, everything is pretty not-good at a minimum

Current API trajectory and project goals (as of Q2 2022):
    - build a performant middle/top layer over several graphics API
    - provide a robust material system which supports asyncronously loading shaders
    - move to a "resource" & "strategy" based architecture?

Xenovisor is being built in a top down approach and only optimized as needed (read: hasn't needed to be optimized at all!). The underlying structures are as simple as they can possibly be (the "super efficient pool allocator" is really just a hash map for now!). I've written some optimizations, like a real generational pool "allocator," in the past, but am holding off on using them so I can actually benchmark my stuff (i.e. pool implementation) against the un-optimized cases.

Potential future features:
 - More advanced APIs will be able to analyze a scene, take information about static/dynamic-ness and batch your models accordingly, but that doesn't exist quite yet.

