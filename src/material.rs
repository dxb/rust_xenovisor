use crate::rgl;
use crate::renderer::{TextureRef, ProgramRef};
use crate::mesh::AttributeFormat;

use crate::gl;
use crate::gl::types::*;
use crate::gl::Gl;

use std::hash::Hash;
use std::ffi::CStr;
use std::borrow::Cow;

pub trait Material {
    /// Data that is per mesh, i.e., every object/mesh in scene requires this if it's to be drawn.
    type MeshData: UniformSet;
    /// Data that is per "scene," i.e., one xenovisor render call.
    ///
    /// You may find it necessary to abstract over this "scene". For example, one scene in your
    /// game/project may generate multiple draw calls that would benefit from different SceneData.
    type SceneData: UniformSet;
    /// Data that is per material.
    ///
    /// It is Clone because material_uniforms returns Cow. This could be changed in the future.
    /// Please open an issue if implementing Clone is a problem for your type!
    type MaterialData: UniformSet + Clone;

    type ProgramGenerator: GenerateProgram;

    /// Get a program generator that can be used to determine if a suitable shader program already
    /// exists for this material.
    fn program_generator(&self) -> Cow<Self::ProgramGenerator>;

    /// Get the uniform data that describes this Material.
    ///
    /// If your Material type is itself a UniformSet or contains a UniformSet, then you can just
    /// borrow the Material. However, Xenovisor also gives you the option to derive the UniformSet
    /// from the Material, in case you want to separate the user facing Material fields from the
    /// shader variables (i.e. cache shader inputs that are implied/derived from other fields).
    fn material_uniforms(&self) -> Cow<Self::MaterialData>;
}

pub unsafe trait UniformSet {
    /// The number of uniforms in this type.
    ///
    /// This should include texture samplers which need to have units assigned. The
    /// actual texture bindings will be determined at runtime through the tex_{}
    /// methods.
    fn count() -> usize;

    fn name(field_index: usize) -> &'static str;

    // TODO: Don't expose OpenGL
    fn glsl_type(field_index: usize) -> GLenum;

    // TODO: Allow this but do it in a way that isn't horrible
    fn derive<F: FnOnce(*const u8)>(&self, f: F, field_index: usize);

    /// The number of active texture units that need to be bound
    fn tex_count(&self) -> usize {
        0
    }

    /// For a tex_index in [0, tex_count), get the texture unit and TextureRef,
    /// respectively.
    fn tex_ref(&self, _tex_index: usize) -> (u32, &TextureRef) {
        panic!("UniformSet has no textures active")
    }
}

/// A struct that must act as a unique key for a given shader program.
///
/// This struct will use a `ProgramCompiler` to create shader programs from arbitrary GLSL code.
/// Xenovisor will share programs between materials that produce equivalent program generators.
///
/// You should think of this type as representing a shader 1:1, and your shader programs are simply
/// a function of this type.
///
/// # Safety and the Eq Invariant
///
/// Let {A, B}: impl GenerateProgram. Then A == B implies that the shader programs that would be
/// generated from A and B are identical.
///
/// Suppose this is violated:
///  - At worst: B could generate a different program than A yet `A == B`, then Xenovisor will use
///    the same shaders for material A and B, even though B requires a different shader. The result
///    of this is undefined by Xenovisor, and depends on the underlying implementation. This may
///    result in OpenGL performing unsafe reads, and is hence why this trait is unsafe.
///  - At best: A != B ever, and Xenovisor will create a unique program for every material.
///    Depending on your usage patterns, this may cause poor performance, but everything will
///    otherwise be okay (excluding bugs in Xenovsior ;^), of course).
///
/// The Hash trait is required for future proofing.
pub unsafe trait GenerateProgram: Clone + Eq + Hash {
    fn generate(&self, compiler: &mut ProgramCompiler);
}

pub struct ProgramCompiler<'a> {
    shaders: Vec<GLuint>,
    buffer: String,
    gl: &'a Gl,
}

/// A partial GLSL Shader definition.
///
/// Xenovisor will build uniform definitions and vertex shader inputs for you (based on the Material
/// struct and associated types that you provide to the renderer); refer to the relevant docs pages
/// for Materials.
///
/// Leading/trailing whitespace on any field will be ignored (trailing newlines are not necessary).
///
/// # Implementation
///
/// At present, everything is string concatenation and no processing or parsing is done (before
/// sending to the GPU), therefore the actual meaning of this struct's fields are relatively
/// meaningless. In the future this may change, so you should stick to the guidelines presented in
/// the documentation of each field.
///
/// What this means for instance, is that `glsl_flavor` and `glsl_decl` can both contain arbitrary
/// code, including functions and uniform and in/out declarations. You should, however, not assume
/// that this will always be the case (e.g., limit `glsl_decl` to structs and structs only).
///
/// Currently (2018.12.30), the shader passed to OpenGL will be:
///
/// ```
/// glsl_flavor + "\n" + glsl_decl + "\n" + uniforms_and_attrs() + "\n" + glsl
/// ```
pub struct ShaderGenerator<'a> {
    /// GLSL version directive and any other directives preceding shader content.
    ///
    /// You may wish to include #defines and #extension directives.
    pub glsl_flavor: &'a str,
    /// Any struct declarations available to be used in the shader.
    pub glsl_decl: &'a str,
    /// The main body of your GLSL code. Should include stage inputs and outputs, and all functions
    /// needed for the given shader stage (except for vertex shaders, where attribute inputs will
    /// be generated for you).
    pub glsl: &'a str,
}

pub(crate) struct CompiledMaterial<Mat: Material> {
    #[allow(dead_code)]
    pub(crate) raw_material: Mat,
    pub(crate) material_uniforms: Mat::MaterialData,
    pub(crate) program: ProgramRef<Mat::ProgramGenerator>,
}

pub(crate) struct ShaderProgram<G: GenerateProgram> {
    // OPTIMIZATION: use SlicePool refs instead of vecs
    pub(crate) material_uniform_info: Vec<UniformInfo>,
    pub(crate) scene_uniform_info: Vec<UniformInfo>,
    pub(crate) mesh_uniform_info: Vec<UniformInfo>,
    pub(crate) gl_id: GLuint,
    pub(crate) generator: G,
}

pub(crate) struct UniformInfo {
    pub(crate) location: GLint,
    pub(crate) field_index: usize,
    pub(crate) method: unsafe fn(&Gl, GLint, *const u8),
}

impl ProgramCompiler<'_> {
    pub(crate) fn new(gl: &Gl) -> ProgramCompiler<'_> {
        ProgramCompiler {
            shaders: Vec::new(),
            buffer: String::new(),
            gl,
        }
    }

    pub(crate) fn compile(self) -> GLuint {
        rgl::create_linked_program(&self.gl, &self.shaders, true)
            .expect(dbpi!("Failed to generate shader"))
    }

    /// Declare a vertex shader.
    pub fn vertex(&mut self, shader: ShaderGenerator) {
        self.buffer.clear();
        self.buffer.reserve(shader.min_len() + 3);
        self.buffer.push_str(shader.glsl_flavor);
        self.buffer.push_str("\n");
        self.buffer.push_str(shader.glsl_decl);
        self.buffer.push_str("\n");

        // generate the uniforms and attributes
        // TODO

        self.buffer.push_str("\n");

        self.buffer.push_str(shader.glsl);

        let vs = match rgl::create_shader(&self.gl, gl::VERTEX_SHADER, &self.buffer) {
            Ok(vs) => vs,
            Err(err) => {
                println!(dbpi!("Failed to generate vertex shader"));
                panic!("{}", err);
            }
        };
        self.shaders.push(vs);
    }

    /// Declare a fragment shader.
    pub fn fragment(&mut self, shader: ShaderGenerator) {
        self.buffer.clear();
        self.buffer.reserve(shader.min_len() + 3);
        self.buffer.push_str(shader.glsl_flavor);
        self.buffer.push_str("\n");
        self.buffer.push_str(shader.glsl_decl);
        self.buffer.push_str("\n");

        // generate the uniforms and attributes
        // TODO

        self.buffer.push_str("\n");

        self.buffer.push_str(shader.glsl);

        let fs = match rgl::create_shader(&self.gl, gl::FRAGMENT_SHADER, &self.buffer) {
            Ok(fs) => fs,
            Err(err) => {
                println!(dbpi!("Failed to generate fragment shader"));
                panic!("{}", err);
            }
        };
        self.shaders.push(fs);
    }
}

impl ShaderGenerator<'_> {
    fn min_len(&self) -> usize {
        self.glsl.len() + self.glsl_decl.len() + self.glsl_flavor.len()
    }
}

impl<Mat: Material> CompiledMaterial<Mat> {
    pub(crate) fn new(program: ProgramRef<Mat::ProgramGenerator>, raw_material: Mat) -> Self {
        // just copy the Cow; this is probably universally better than branching in the renderer...
        let material_uniforms = raw_material.material_uniforms()
            .into_owned();
        CompiledMaterial {
            raw_material,
            material_uniforms,
            program,
        }
    }

    // TODO: Update this
    pub(crate) fn assert_compatibility(&self, _fmt: &AttributeFormat) {
        /*
        assert!(
            !self.material.use_vertex_colors || fmt.colors.is_some(),
            "Material requires vertex colors but mesh does not supply any",
        );*/
    }
}

impl<G: GenerateProgram> ShaderProgram<G> {
    pub(crate) fn new<Mat: Material>(gl: &Gl, program: GLuint, generator: G) -> Self {
        let mut material_uniform_info = Vec::new();
        load_uniforms::<Mat::MaterialData>(gl, program, &mut material_uniform_info);
        let mut scene_uniform_info = Vec::new();
        load_uniforms::<Mat::SceneData>(gl, program, &mut scene_uniform_info);
        let mut mesh_uniform_info = Vec::new();
        load_uniforms::<Mat::MeshData>(gl, program, &mut mesh_uniform_info);
        ShaderProgram {
            material_uniform_info,
            scene_uniform_info,
            mesh_uniform_info,
            gl_id: program,
            generator,
        }
    }
}

/// This implementation may be useful for getting started with Xenovisor.
unsafe impl UniformSet for () {
    fn count() -> usize {
        0
    }

    fn name(_field_index: usize) -> &'static str {
        unreachable!();
    }

    // TODO: Don't expose OpenGL
    fn glsl_type(_field_index: usize) -> GLenum {
        unreachable!();
    }

    // TODO: Allow this but do it in a way that isn't horrible
    fn derive<F: FnOnce(*const u8)>(&self, _f: F, _field_index: usize) {
        unreachable!();
    }
}

pub(crate) fn load_uniforms<U: UniformSet>(
    gl: &Gl,
    program: GLuint,
    uniforms: &mut Vec<UniformInfo>
) {
    let mut buffer: Vec<u8> = Vec::new();
    for field_index in 0..U::count() {
        buffer.clear();
        buffer.extend(U::name(field_index).bytes());
        buffer.push(0);
        let location = unsafe {
            gl.GetUniformLocation(
                program,
                CStr::from_bytes_with_nul(&buffer)
                    .expect("A UniformSet returned a name with an interior null byte; \
                        names should be ASCII")
                    .as_ptr(),
            )
        };
        let method = match U::glsl_type(field_index) {
            gl::FLOAT_VEC2 => upload_vec2,
            gl::FLOAT_VEC3 => upload_vec3,
            gl::FLOAT_VEC4 => upload_vec4,
            gl::FLOAT_MAT4 => upload_mat4,
            gl::INT => upload_int,
            _ => panic!("Unknown GLSL type"),
        };
        uniforms.push(UniformInfo {
            field_index,
            location,
            method,
        });
    }
}

unsafe fn upload_vec2(gl: &Gl, location: GLint, value: *const u8) {
    gl.Uniform2fv(location, 1, value as *const _);
}

unsafe fn upload_vec3(gl: &Gl, location: GLint, value: *const u8) {
    gl.Uniform3fv(location, 1, value as *const _);
}

unsafe fn upload_vec4(gl: &Gl, location: GLint, value: *const u8) {
    gl.Uniform4fv(location, 1, value as *const _);
}

unsafe fn upload_mat4(gl: &Gl, location: GLint, value: *const u8) {
    gl.UniformMatrix4fv(location, 1, gl::FALSE, value as *const _);
}

unsafe fn upload_int(gl: &Gl, location: GLint, value: *const u8) {
    gl.Uniform1i(location, *value as _);
}
