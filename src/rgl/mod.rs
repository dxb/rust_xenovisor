//! Inspired by the (author's!) crate `rustic_gl`. Most of the code has been ripped from there and
//! altered for struct-generated GL.
//!
//! `rgl` is either `rustic_gl` or `raw_gl`, take your pick...

use crate::gl;
use crate::gl::types::*;
use crate::gl::Gl;

mod error;

pub use self::error::*;

// TODO: Allow turning this off via feature
#[macro_export]
macro_rules! gl_panic_on_error {
    ($gl:expr, $msg:expr) => {{
        let mut error = false;
        while let Some(err) = $crate::rgl::get_error($gl) {
            error = true;
            eprintln!("{}", err);
        }
        if error {
            panic!(concat!("Encountered OpenGL error(s); ", $msg));
        }
    }};
    ($gl:expr) => {
        gl_panic_on_error!($gl, "")
    };
}

/// Should be called in a loop in order to get all errors!
pub fn get_error(gl: &Gl) -> Option<GlError> {
    let error = unsafe { gl.GetError() };
    if error == gl::NO_ERROR {
        return None;
    }
    Some(match error {
        gl::INVALID_ENUM => GlError::GL_INVALID_ENUM,
        gl::INVALID_VALUE => GlError::GL_INVALID_OPERATION,
        gl::INVALID_OPERATION => GlError::GL_INVALID_OPERATION,
        gl::OUT_OF_MEMORY => GlError::GL_OUT_OF_MEMORY,
        // TODO: There's more error types to cover
        code => GlError::UnknownError(code),
    })
}

pub fn create_vao(gl: &Gl) -> GlResult<GLuint> {
    unsafe {
        let mut vao = 0;
        gl.GenVertexArrays(1, &mut vao);
        if vao == 0 {
            return Err(GlError::VaoCreation);
        }
        Ok(vao)
    }
}

pub fn create_buffer(gl: &Gl) -> GlResult<GLuint> {
    unsafe {
        let mut b = 0;
        gl.GenBuffers(1, &mut b);
        if b == 0 {
            return Err(GlError::BufferCreation);
        }
        Ok(b)
    }
}

macro_rules! get_info_log {
    ($gl:expr, $get_attr:ident, $get_log:ident, $gl_id:expr) => {{
        let mut log_length_glint: GLint = 0;
        $gl.$get_attr($gl_id, gl::INFO_LOG_LENGTH, &mut log_length_glint);
        let log_length = log_length_glint as usize;
        if log_length == 0 {
            None
        } else {
            let mut raw_log = Vec::<u8>::with_capacity(log_length);
            $gl.$get_log($gl_id, log_length as GLsizei,
                0 as *mut GLsizei, raw_log.as_mut_ptr() as *mut GLchar);
            raw_log.set_len(log_length);
            let log = String::from_utf8(raw_log)
                .expect("OpenGL returned invalid utf8 in a program info log");
            Some(log)
        }
    }}
}

pub fn create_program(gl: &Gl) -> GlResult<GLuint> {
    let gl_id = unsafe { gl.CreateProgram() };
    if gl_id == 0 {
        Err(GlError::ProgramCreation)
    } else {
        Ok(gl_id)
    }
}

pub fn create_shader(gl: &Gl, kind: GLenum, source: &str) -> GlResult<GLuint> {
    unsafe {
        let gl_id = gl.CreateShader(kind as _);
        if gl_id == 0 {
            return Err(GlError::ShaderCreation);
        }
        gl.ShaderSource(
            gl_id,
            1,
            &(source.as_ptr() as *const _),
            &(source.len() as _)
        );
        gl.CompileShader(gl_id);

        let mut status = 0;
        gl.GetShaderiv(gl_id, gl::COMPILE_STATUS, &mut status);
        if status != 1 {
            Err(GlError::ShaderCompilation(
                get_info_log!(gl, GetShaderiv, GetShaderInfoLog, gl_id)
            ))
        } else {
            Ok(gl_id)
        }
    }
}

pub fn get_link_status(gl: &Gl, program_id: GLuint) -> GlResult<()> {
    let mut link_status = gl::FALSE as i32;
    unsafe {
        gl.GetProgramiv(
            program_id,
            gl::LINK_STATUS,
            &mut link_status as *mut _
        );
        if link_status != gl::TRUE as i32 {
            Err(GlError::ProgramLinkage(
                get_info_log!(gl, GetProgramiv, GetProgramInfoLog, program_id)
            ))
        } else {
            Ok(())
        }
    }
}

/// Create an OpenGL program given a slice of shader references.
///
/// Pass `true` for `delete_shaders` in order to automatically delete each shader after linking.
pub fn create_linked_program(gl: &Gl, shaders: &[GLuint], delete_shaders: bool) -> GlResult<GLuint> {
    let program = create_program(gl)?;
    unsafe {
        for &shader in shaders {
            gl.AttachShader(program, shader);
        }
        gl.LinkProgram(program);
        get_link_status(gl, program)?;
        // we have to detach the shaders before the shader objects will be freed
        for &shader in shaders {
            gl.DetachShader(program, shader);
            if delete_shaders {
                gl.DeleteShader(shader);
            }
        }
    }
    Ok(program)
}
