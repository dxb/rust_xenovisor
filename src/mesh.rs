use crate::pool::{
    SlicePoolRef,
    PoolRef,
};

use crate::renderer::MaterialRef;

use crate::gl;
use crate::gl::Gl;
use crate::gl::types::*;

pub mod prelude {
    //! Everything needed to create a mesh.

    pub use super::{
        BufferFormat,
        BufferLayout,
        Sva,
        SvaType,
    };
}

pub type VboRef = SlicePoolRef<GLuint, u32>;
pub type AttributeFormatRef = PoolRef<AttributeFormat>;
pub type MeshPartRef = SlicePoolRef<MeshPart, u32>;

/// A single mesh which may consist of multiple materials (one material per part).
///
/// A conceptual mesh and everything needed to draw it; a mesh part is enough but in practice many
/// "meshes" (as a designer thinks of them) use multiple materials, and hence a "mesh" is really
/// several meshes (one per material), and we are calling these sub meshes "mesh parts."
pub struct Mesh {
    pub(crate) parts: MeshPartRef,
}

pub struct MeshPart {
    pub(crate) attribute_format: AttributeFormatRef,
    pub(crate) vbos: VboRef,
    pub(crate) vao: GLuint,
    pub(crate) ibo: GLuint,
    pub(crate) ibo_type: GLenum,
    pub(crate) elements: isize,
    pub(crate) material: MaterialRef,

    // If this mesh is dynamic it will have client-side backing storage.
    // data: Option<Box<MeshStorage>>,
}

impl MeshPart {
    pub(crate) fn constrain_elements(&mut self, elements: usize) {
        if self.elements < 0 {
            // element count is currently unset
            self.elements = elements as isize;
        } else {
            self.elements = ::std::cmp::min(self.elements, elements as isize);
        }
    }

    pub(crate) fn set_elements(&mut self, elements: usize) {
        // OpenGL checks this but if we disable OpenGL error checking then this
        //      could be missed in release mode
        assert!(elements < isize::max_value() as usize);
        self.elements = elements as isize;
    }
}

// TODO: Should we take Copy off this? It's gonna get pretty huge!
// TODO: Real support for interleaved buffers
/// Describes the layout of a meshes attributes.
///
/// If an attribute is present, the tuple specifies which VBO is used and the format of the VBO.
/// The VBO is specified using an index into the meshes corresponding VBO slice storage, not the
/// OpenGL name of the buffer object.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub struct AttributeFormat {
    pub(crate) pos: Option<(usize, BufferFormat)>,
    pub(crate) normals: Option<(usize, BufferFormat)>,
    pub(crate) colors: Option<(usize, BufferFormat)>,
    pub(crate) uvs: Option<(usize, BufferFormat)>,
}

pub struct AttributeFormatIter<'a>([Option<&'a (usize, BufferFormat)>; 4], usize);

/// Describes the layout of vertex data in a buffer object.
///
/// A BufferFormat will be created for each attribute associated with a glTF primitive, and each
/// one will merge together all the relevant information from the associated accessor and
/// bufferView. Only the bufferView's stride is relevant, as its offset is only used to access the
/// underlying buffer store in the glTF object. The stride relates to how far apart each
/// semantic element is from its successor in the buffer (or equivalently, how wide each vertex
/// attribute is).
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub struct BufferFormat {
    pub kind: SvaType,
    pub normalized: bool,
    pub components: u32,
    pub layout: BufferLayout,
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum BufferLayout {
    Packed,
    Interleaved { stride: usize, offset: usize },
}

/// Vertex attribute type
macro_rules! generate_sva_type {
    (
        $(
            #[$m:meta]
        )*
        pub enum $struct:ident {
            $(
                $name:ident => $gl_enum:expr => bytes: $size_bytes:expr
            ),+,
        }
    ) => {
        $(
            #[$m]
        )*
        pub enum $struct {
            $($name),+,
        }

        impl $struct {
            pub fn as_glenum(&self) -> GLenum {
                match self {
                    $(
                        $struct::$name => $gl_enum
                    ),+,
                }
            }

            pub fn size_bytes(&self) -> usize {
                match self {
                    $(
                        $struct::$name => $size_bytes
                    ),+,
                }
            }
        }
    }
}

generate_sva_type! {
    #[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
    pub enum SvaType {
        F32 => gl::FLOAT => bytes: 4,
        I16 => gl::SHORT => bytes: 2,
        U32 => gl::UNSIGNED_INT => bytes: 4,
        U16 => gl::UNSIGNED_SHORT => bytes: 2,
        I8 => gl::BYTE => bytes: 1,
        U8 => gl::UNSIGNED_BYTE => bytes: 1,
    }
}

/// Represents a type that can be used in an index buffer
pub trait IndexBufferTypeInstance {
    fn sva_type() -> SvaType;
}

impl IndexBufferTypeInstance for u32 {
    fn sva_type() -> SvaType { SvaType::U32 }
}

impl IndexBufferTypeInstance for u16 {
    fn sva_type() -> SvaType { SvaType::U16 }
}

impl IndexBufferTypeInstance for u8 {
    fn sva_type() -> SvaType { SvaType::U8 }
}

pub trait SvaTypeInstance {
    fn sva_type() -> SvaType;
}

impl<T: IndexBufferTypeInstance> SvaTypeInstance for T {
    fn sva_type() -> SvaType {
        T::sva_type()
    }
}

impl SvaTypeInstance for f32 {
    fn sva_type() -> SvaType { SvaType::F32 }
}


impl SvaTypeInstance for i16 {
    fn sva_type() -> SvaType { SvaType::I16 }
}

/// This type is provided as a convenience for specifying formats. You can invoke it with the UFCS
/// like so:
///
/// ```rust
/// <[f32; N] as VertexAttribute>::format()
/// ```
///
/// where `N` is the number of elements in the array. This trait is implemented for some common
/// sizes.
///
/// # Safety
///
/// This trait is unsafe as the BufferFormat returned from it will most likely be used by safe APIs
/// to uphold assumptions and calculate offsets; getting it wrong could therefore make your code
/// unsafe.
pub unsafe trait VertexAttribute {
    fn format() -> BufferFormat;
}

macro_rules! impl_VertexAttribute_for {
    ($kind:ty, $n:expr) => {
        unsafe impl VertexAttribute for [$kind; $n]
        {
            fn format() -> BufferFormat {
                BufferFormat {
                    kind: <$kind as SvaTypeInstance>::sva_type(),
                    layout: BufferLayout::Packed,
                    components: $n,
                    normalized: false,
                }
            }
        }
    }
}

impl_VertexAttribute_for!(f32, 1);
impl_VertexAttribute_for!(f32, 2);
impl_VertexAttribute_for!(f32, 3);
impl_VertexAttribute_for!(f32, 4);

/// Identifies a vertex attribute semantically (instead of by index or layout)
#[derive(Debug, Copy, Clone)]
pub enum Sva {
    Position,
    Normal,
    Color,
    Uv,
}

impl AttributeFormat {
    pub(crate) fn iter<'a>(&'a self) -> impl Iterator<Item=&'a (usize, BufferFormat)> + 'a {
        let iter = AttributeFormatIter([
            self.pos.as_ref(),
            self.normals.as_ref(),
            self.uvs.as_ref(),
            self.colors.as_ref(),
        ], 0);
        iter
    }
}

impl<'a> Iterator for AttributeFormatIter<'a> {
    type Item = &'a (usize, BufferFormat);

    fn next(&mut self) -> Option<Self::Item> {
        let mut index = self.1;
        loop {
            if index == self.0.len() {
                return None;
            }

            let result = self.0[index];
            if let Some(result) = result {
                self.1 = index + 1;
                return Some(result);
            }
            index += 1;
        }
    }
}

impl BufferFormat {
    pub fn element_size_bytes(&self) -> usize {
        self.kind.size_bytes() * (self.components as usize)
    }

    /// Asserts that this buffer format is plausible.
    ///
    /// i.e. it's layout is packed or:
    ///  - the layout is valid, and
    ///  - the size of the element is less than stride - offset
    pub fn assert_valid(&self) {
        let element_size = self.element_size_bytes();
        assert!(
            !self.normalized || self.kind != SvaType::F32,
            "Only integer types may be normalized"
        );
        match self.layout {
            BufferLayout::Interleaved { stride, offset } => {
                assert_ne!(stride, 0, "Stride must not be zero if an interleaved layout is used");
                assert!(offset < stride, "Offset must be less than a vertex elements stride.");
                assert!(
                    element_size <= stride - offset,
                    "Element size must be less than stride - offset",
                );
            },
            _ => {},
        }
    }

    pub fn vertex_attrib_pointer_stride(&self) -> usize {
        match self.layout {
            BufferLayout::Packed => 0,
            BufferLayout::Interleaved { stride, .. } => stride,
        }
    }

    pub fn vertex_attrib_pointer_offset(&self) -> usize {
        match self.layout {
            BufferLayout::Packed => 0,
            BufferLayout::Interleaved { offset, .. } => offset,
        }
    }
}

impl Sva {
    pub(crate) fn array_index(&self) -> u32 {
        match self {
            Sva::Position => 0,
            Sva::Normal => 1,
            // Sva::Tangent => 2,
            Sva::Color => 3,
            Sva::Uv => 4,
        }
    }
}

pub(crate) unsafe fn upload_mesh_vbo<T>(
    gl: &Gl,
    format: &BufferFormat,
    buffer: *const T,
    buffer_byte_len: usize,
    elements: usize,
    target: GLenum,
    flag: GLenum,
) {
    // OpenGL checks this but if we disable OpenGL error checking then this
    //      could be missed in release mode
    assert!(elements < isize::max_value() as usize);

    // make this method slightly more safe
    assert_eq!(
        format.element_size_bytes() * elements,
        buffer_byte_len,
        "Supplied buffer length is different from anticipated length"
    );
    gl.BufferData(
        target,
        buffer_byte_len as _,
        buffer as *const _,
        flag,
    );
}

pub(crate) unsafe fn upload_buffer_format(
    gl: &Gl,
    array_index: GLuint,
    format: &BufferFormat,
) {
    gl.EnableVertexAttribArray(array_index);
    gl.VertexAttribPointer(
        array_index,
        format.components as _,
        format.kind.as_glenum(),
        format.normalized as _,
        format.vertex_attrib_pointer_stride() as _,
        format.vertex_attrib_pointer_offset() as *const _,
    );
}

/*
pub trait MeshStorage {
    fn attribute(attribute: Vat, loader: &mut MeshLoader);
    /// TODO: Dependency inversion, someone will call us and we will tell them we're invalid.
    fn is_invalid(attribute: Vat) -> bool;
}
*/

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic]
    fn test_buffer_format_floats_cant_be_normalized() {
        BufferFormat {
            kind: SvaType::F32,
            normalized: true,
            components: 1,
            layout: BufferLayout::Packed,
        }.assert_valid();
    }

    #[test]
    #[should_panic]
    fn test_buffer_format_bad_stride() {
        BufferFormat {
            kind: SvaType::F32,
            normalized: false,
            components: 3,
            layout: BufferLayout::Interleaved { offset: 0, stride: 0 },
        }.assert_valid();
    }

    #[test]
    #[should_panic]
    fn test_buffer_format_bad_offset() {
        BufferFormat {
            kind: SvaType::F32,
            normalized: false,
            components: 3,
            layout: BufferLayout::Interleaved { offset: 64, stride: 24 },
        }.assert_valid();
    }

    #[test]
    #[should_panic]
    fn test_buffer_format_bad_element_size() {
        BufferFormat {
            kind: SvaType::F32,
            normalized: false,
            components: 3,
            layout: BufferLayout::Interleaved { offset: 12, stride: 15 },
        }.assert_valid();
    }

    #[test]
    fn test_buffer_format_common_color_format_passes() {
        BufferFormat {
            kind: SvaType::U8,
            normalized: true,
            components: 3,
            layout: BufferLayout::Packed,
        }.assert_valid();
    }

    #[test]
    fn test_buffer_format_homogeneous_passes() {
        BufferFormat {
            kind: SvaType::F32,
            normalized: false,
            components: 4,
            layout: BufferLayout::Packed,
        }.assert_valid();
    }

    #[test]
    fn test_buffer_format_interleaved_pos_normals_colors_passes() {
        let stride = (4 * 4) + (3 * 2) + (4 * 1);
        let offsets = (0, 16, 16 + 6);

        // pos
        BufferFormat {
            kind: SvaType::F32,
            normalized: false,
            components: 4,
            layout: BufferLayout::Interleaved { stride, offset: offsets.0 },
        }.assert_valid();

        // normals
        BufferFormat {
            kind: SvaType::I16,
            normalized: true,
            components: 3,
            layout: BufferLayout::Interleaved { stride, offset: offsets.1 },
        }.assert_valid();

        // colors
        BufferFormat {
            kind: SvaType::U8,
            normalized: true,
            components: 4,
            layout: BufferLayout::Interleaved { stride, offset: offsets.2 },
        }.assert_valid();
    }
}
